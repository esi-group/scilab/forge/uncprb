// Copyright (C) 2010-2011 - DIGITEO - Michael Baudin

// Updates the .xml files by deleting existing files and
// creating them again from the .sci with help_from_sci.


path = get_absolute_file_path("update_help.sce");

//
// Updating the main library
helpdir = path;
mprintf("Updating %s...\n",helpdir);
funarray = [
  "uncprb_getfunc"
  "uncprb_getinitf"
  "uncprb_getinitpt"
  "uncprb_getobjfcn"
  "uncprb_getvecfcn"
  "uncprb_getvecjacfd"
  "uncprb_getvecjac"
  "uncprb_getgrdfcn"
  "uncprb_getgrdfd"
  "uncprb_gethesfcn"
  "uncprb_gethesfd"
  "uncprb_getopt"
  "uncprb_getproblems"
  "uncprb_getclass"
  "uncprb_getname"
  ];
macrosdir = fullfile(path,"..","..","macros");
demosdir = [];
modulename = "uncprb";
helptbx_helpupdate ( funarray , helpdir , macrosdir , demosdir , modulename , %t );
//
// Updating the benchmark library
helpdir = fullfile(path,"benchmark");
mprintf("Updating %s...\n",helpdir);
funarray = [
  "uncprb_optimfun"
  "uncprb_optimget"
  "uncprb_optiminit"
  "uncprb_lsqrsolvefun"
  "uncprb_lsqrsolvejac"
  "uncprb_lsqrsolveget"
  "uncprb_lsqrsolveinit"
  "uncprb_leastsqfun"
  "uncprb_leastsqjac"
  "uncprb_leastsqinit"
  "uncprb_leastsqget"
  "uncprb_computestatus"
  "uncprb_printsummary"
  ];
macrosdir = fullfile(path,"..","..","macros");
demosdir = [];
modulename = "uncprb";
helptbx_helpupdate ( funarray , helpdir , macrosdir , demosdir , modulename , %t );

