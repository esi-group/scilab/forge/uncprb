// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function uncprb_leastsqinit()
    // Initialize the internal data for uncprb_leastsqfun
    //
    // Calling Sequence
    //   uncprb_leastsqinit()
    //
    // Parameters
    //
    //
    // Description
    // Initialize the internal data used by uncprb_leastsqfun and
    // uncprb_leastsqjac.
    //
    // Examples
    //   // See uncprb_leastsqfun
    //
    // Authors
    //   Michael Baudin - 2011 - DIGITEO

    global __LEASTSQDATA__

    __LEASTSQDATA__.funeval = 0
    __LEASTSQDATA__.Jeval = 0
endfunction

