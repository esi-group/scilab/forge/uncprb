// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_pen1(n,m,x,option)
  // Penalty I  function
  // -------------------
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // function [fvec,J]=pen1(n,m,x,option)
  // Dimensions -> n=variable, m=n+1
  // Problem no. 23
  // Standard starting point -> x=(s(j)) where
  //                            s(j)=j
  // Minima -> f=2.24997...10^(-5)  if n=4
  //           f=7.08765...10^(-5)  if n=10
  //
  // 11/21/94 by Madhu Lamba
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_pen1",option);
  end
  //
  apifun_checktrue ( m==n+1 , msprintf(gettext("%s: Expected m==n+1, but got the contrary. m=%d, n=%d"), "uncprb_pen1" , m , n ))
  //
  fvec=[]
  J=[]
  H = []
  //
  if ( option==2 | option==3 | option==4) then
    J = zeros(m,n);
  end
  for i = 1:n

    if ( option==1 | option==3 | option==4) then
      fvec(i) = sqrt(0.00001)*(x(i)-1);
    end

    if ( option==2 | option==3 | option==4) then
      J(i,i) = sqrt(0.00001);
    end
  end

  if ( option==1 | option==3 | option==4) then
    total = 0;
    for j = 1:n
      total = total+x(j)'*x(j);
    end

    fvec(n+1) = total-1/4
  end

  if ( option==2 | option==3 | option==4 ) then
    for j = 1:n
      J(n+1,j) = 2*x(j);
    end
  end
  //
  if ( option==4 ) then
    t1 = -2.5D-1
    for j = 1: n
      t1 = t1 + x(j)**2
    end
    d1 = 2*1.0D-5
    th = 4*t1
    im = 0
    for j = 1: n
      hesd(j) = d1 + th + 8*x(j)**2
      for k = 1: j-1
        im = im + 1
        hesl(im) = 8*x(j)*x(k)
      end
    end
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

