// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_sing(n,m,x,option)

  // Function [fvec,J]= sing(n,m,x,option)
  // Powell singular function  [13]
  // Dimensions:    n=4    m=4
  // Function definitions:
  //       f1(x)=x1 + 10x2
  //       f2(x)= 5^.5*(x3 - x4)
  //       f3(x)= (x2-2x3)^2
  //       f4(x)= 10^.5*(x1-x4)^2
  // Starting point: (3,-1,0,1)
  // Minima of f=0 at the origin
  //
  // Coded in Matlab  October 31   PLK
  // **************************************8

  if ( option<1 | option>3 ) then
    msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_sing",option);
  end
  //
  fvec=[];
  J=[];
  //
  if ( option==1 | option==3 ) then
    fvec = [x(1)+10*x(2);sqrt(5)*(x(3)-x(4));(x(2)-2*x(3))^2;sqrt(10)*((x(1)-x(4))^2)];
  end

  if ( option==2 | option==3 ) then
    J = [1,10,0,0
    0,0,sqrt(5),-sqrt(5)
    0,2*(x(2)-2*x(3)),-4*(x(2)-2*x(3)),0
    2*sqrt(10)*(x(1)-x(4)),0,0,-2*sqrt(10)*(x(1)-x(4))];
  end
endfunction
function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

