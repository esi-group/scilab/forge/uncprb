// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_badscb(n,m,x,option)

  // Brown badly scaled function
  // ---------------------------
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // function [fvec,J]=badscb(n,m,x,option)
  // Problem no. 4
  // Dimensions -> n=2, m=3
  // Standard starting point -> x=(1,1)
  // Minima -> f=0 at (1e+6,2e-6)
  //
  // Revised on 10/22/94 by Madhu Lamba
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_osb2",option);
  end
  //
  fvec=[]
  J=[]
  H=[]
  //
  if ( option==1 | option==3 | option==4 ) then
    fvec = [x(1)-(10^6);x(2)-0.000002;x(1)*x(2)-2];
  end
  if ( option==2 | option==3 | option==4 ) then
    J = [1,0;0,1;x(2),x(1)];
  end
  //
  if ( option==4 ) then
    hesd(1) = 2 + 2*x(2)**2
    hesd(2) = 2 + 2*x(1)**2
    hesl(1) = 4*x(1)*x(2) - 4
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

