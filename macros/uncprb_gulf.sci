// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_gulf(n,m,x,option)


  // ***************************************************
  // ***************************************************
  // function [fvec, J]= gulf(n,m,x,option)
  // Gulf research and development function      [11]
  // Dimensions  n=3,  n=<m=<100
  // Function definition:
  //       f(x)= exp[-(| y(i)mi x(2)|^x(3) / x(1))- t(i)
  //       where t(i) = i/100
  //       and y(i)=25 +(-50 ln(t(i))^2/3)
  // Standard starting point x=(5,2.5,0.15)
  // minima of f=0 at (50,25,1.5)
  //
  // Revised 10/23/94      PLK
  // ***************************************************
  // TODO : vectorize this

  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_gulf",option);
  end
  //
  apifun_checktrue ( m>=n , msprintf(gettext("%s: Expected m>=n, but got the contrary. m=%d, n=%d"), "uncprb_gulf" , m , n ))
  apifun_checktrue ( m<=100 , msprintf(gettext("%s: Expected m<=100, but got the contrary. m=%d"), "uncprb_gulf" , m ))
  //
  fvec=[]
  J=[]
  H=[]
  //
  x1 = x(1);
  x2 = x(2);
  x3 = x(3);
  if ( x1==0 ) then
    warning(msprintf("%s: Singularity in gulf function evaluation","uncprb_gulf"));
  end
  x1inv = 1/x1;
  for i = 1:m
    ti = i*0.01;
    yi = 25+(-50*log(ti))^(2/3);
    ai = yi-x2;

    if ( option==1 | option==3 | option==4 ) then
      fvec(i) = exp(-(abs(ai)^x3)/x1)-ti;
    end

    if ( option==2 | option==3 | option==4 ) then
      av = abs(ai);
      bi = av^x3;
      ci = bi*x1inv;
      ei = exp(-ci);
      d1 = ci*x1inv;
      d2 = x3*x1inv*(av^(x3-1));
      d3 = -log(av)*ci;
      J(i,1) = d1*ei;
      if ai>=0 then
        J(i,2) = d2*ei;
      else
        J(i,2) = -d2*ei;
      end
      J(i,3) = d3*ei;
    end
  end
  //
  if ( option==4 ) then
    hesd(1: 3) = 0
    hesl(1: 3) = 0
    d1 = 2/3
    for i = 1:m
      arg = i/100
      r = (-50*log(arg))**d1+25-x(2)
      t1 = abs(r)**x(3)/x(1)
      t2 = exp(-t1)
      t3 = t1 * t2 * (t1*t2+(t1-1)*(t2-arg))
      t = t1 * t2 * (t2-arg)
      logr = log(abs(r))
      hesd(1) = hesd(1) + t3 - t
      hesd(2) = hesd(2) + (t+x(3)*t3)/r**2
      hesd(3) = hesd(3) + t3*logr**2
      hesl(1) = hesl(1) + t3/r
      hesl(2) = hesl(2) - t3*logr
      hesl(3) = hesl(3) + (t-x(3)*t3*logr)/r
    end
    hesd(1) = hesd(1) / x(1)**2
    hesd(2) = hesd(2) * x(3)
    hesl(1) = hesl(1) * x(3)/x(1)
    hesl(2) = hesl(2) / x(1)
    hesd(1: 3) = 2 * hesd(1: 3)
    hesl(1: 3) = 2 * hesl(1: 3)
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

