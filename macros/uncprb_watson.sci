// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.
function [fvec,J,H]=uncprb_watson(n,m,x,option)

  //  Function [fvec,J]= watson(n,m,x,option)
  //  Watson function    [20]
  //  Dimensions : n=20,  m=31
  //  Standard starting point : (0,....,0)
  //  Minima of f=2.28767...10^(-3)    if n=6
  //            f=1.39976...10^(-6)    if n=9
  //            f=4.72238...10^(-10)   if n=12
  //      f=2.48631...10^(-20)   if n=20
  //
  // Revised  11/94        PLK
  // **************************************************

  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_watson",option);
  end
  //
  apifun_checktrue ( m==31 , msprintf(gettext("%s: Expected m==31, but got the contrary. m=%d"), "uncprb_watson" , m ))
  apifun_checktrue ( n>=2 , msprintf(gettext("%s: Expected n>=2, but got the contrary. n=%d"), "uncprb_watson" , n ))
  apifun_checktrue ( n<=31 , msprintf(gettext("%s: Expected n<=31, but got the contrary. n=%d"), "uncprb_watson" , n ))
  //
  fvec=[];
  J=[];
  H=[]
  //
  t = (1:29)'./29
  for i = 1:29
    sum1 = sum ( ((2:n)'-1).*(x(2:n).*(t(i).^((2:n)'-2))) )
    sum2 = sum ( x(1:n).*(t(i).^((1:n)'-1)) )
    if ( option==1 | option==3 | option==4 ) then
      fvec(i) = sum1-(sum2^2)-1;
    end
    if ( option==2 | option==3 | option==4 ) then
      J(i,1) = -2*sum2;
      J(i,2:n) = ((2:n)-1).*(t(i).^((2:n)-2))-2*sum2*(t(i).^((2:n)-1));
    end
  end
  if ( option==1 | option==3 | option==4 ) then
    fvec(30:31) = [x(1) x(2)-(x(1)^2)-1]'
  end
  if ( option==2 | option==3 | option==4 ) then
    J(30,1) = 1;
    J(30,2:n) = 0;
    J(31,1:2) = [-2*x(1) 1];
    J(31,3:n) = 0;
  end
  //
  if ( option==4 ) then
    hesd(1: n) = 0
    hesl(1: n*(n-1)/2) = 0
    for i = 1: 29
      d1 = i/29
      d2 = 1
      s1 = 0
      s2 = x(1)
      for j = 2: n
        s1 = s1 + (j-1)*d2*x(j)
        d2 = d1*d2
        s2 = s2 + d2*x(j)
      end
      t = 2 * (s1-s2**2-1) * d1**2
      s3 = 2*d1*s2
      d2 = 1/d1
      im = 0
      for j = 1: n
        t1 = (j-1) - s3
        hesd(j) = hesd(j) + (t1**2-t)*d2**2
        d3 = 1/d1
        for k = 1: j-1
          im = im + 1
          hesl(im) = hesl(im) + (t1*((k-1)-s3) - t) * d2*d3
          d3 = d1*d3
        end
        d2 = d1*d2
      end
    end
    t3 = x(2) - x(1)**2 - 1
    hesd(1) = hesd(1) + 1 - 2*(t3-2*x(1)**2)
    hesd(2) = hesd(2) + 1
    hesl(1) = hesl(1) - 2*x(1)
    hesd(1: n) = 2 * hesd(1: n)
    hesl(1: n*(n-1)/2) = 2 * hesl(1: n*(n-1)/2)
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

