// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function result = uncprb_getproblems(varargin)
  // Lists the problems.
  //
  // Calling Sequence
  //   nprobmax = uncprb_getproblems()
  //   str = uncprb_getproblems(nprob)
  //   str = uncprb_getproblems(prbmat,verbose)
  //
  // Parameters
  //   nprobmax: a floating point integer, the maximum problem number. Valid problem numbers are then produced by 1:nprobmax.
  //   prbmat: a matrix of floating point integers, the problem numbers
  //   verbose: a boolean, if true then prints the problem description (default = true).
  //   str: a string describing the problem
  //
  // Description
  // This function lists the respective information associated with the test
  // function number in prbmat. There is a direct map from the problem
  // name to the function name. For example, the "ROSE" problem corresponds to the
  // "uncprb_rose" function which is in the "uncprb_rose.sci" file.
  // The numbers in parenthesis (be it n or m) means that the files include
  // parameters for varying the dimension of the problem.
  // Be sure to read the paper "Algorithm 566)" so that the parameters
  // n and m are consistent with the problem.
  //
  // Examples
  //   // Display one problem at a time
  //   uncprb_getproblems(1);
  //   uncprb_getproblems(35);
  //
  //   // Display all problems
  //   nprobmax = uncprb_getproblems()
  //   uncprb_getproblems(1:nprobmax);
  //
  //   // Get all problems
  //   nprobmax = uncprb_getproblems()
  //   str = uncprb_getproblems(1:nprobmax,%f);
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //   Scilab port: 2000-2004, Benoit Hamelin, Jean-Pierre Dussault
  //   Matlab port: 1994, Chaya Gurwitz, Livia Klein, Madhu Lamba
  //   Fortran 77: 1981, More, Garbow, Hillstrom

  [lhs, rhs] = argn();
  if ( rhs > 2 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while at most 1 is expected."), "uncprb_getproblems", rhs);
    error(errmsg)
  end
  //
  if (rhs == 0) then
    if ( lhs <> 1 ) then
      errmsg = msprintf(gettext("%s: Unexpected number of output arguments : %d provided while %d is expected."), "uncprb_getproblems", rhs,1);
      error(errmsg)
    end
    result = 35
    return;
  end
  //
  prbmat = varargin(1)
  if ( rhs < 2 ) then
    verbose = %t
  else
    verbose = varargin(2)
  end
  //
  if ( size(prbmat,"r") <> 1 ) then
    errmsg = msprintf(gettext("%s: Unexpected number rows in prbmat : %d provided while 1 is expected."), "uncprb_getname", size(prbmat,"r"));
    error(errmsg)
  end
  //
  str(1) = "No.    file      n    m    Name"
  str(2) = "---    ----      -    -    ----"
  k = 3
  //
  // Get the variable problems
  [prb_n,prb_m] = uncprb_getvarprbs()
  //
  // Make the report
  for nprob = prbmat
    [n,m,x0]=uncprb_getinitf(nprob)
    sname = uncprb_getname ( nprob )
    uname = convstr(sname,"u")
    lname = uncprb_getname ( nprob , %t )
    if ( find(nprob==prb_n) <> [] ) then
      str_n = "(" + string(n) + ")"
    else
      str_n = string(n)
    end
    if ( find(nprob==prb_m) <> [] ) then
      str_m = "(" + string(m) + ")"
    else
      str_m = string(m)
    end
    str(k) = msprintf("# %2d.  %-9s %-4s %-4s %-10s",..
      nprob,uname,str_n,str_m,lname)
    k = k + 1
  end
  if ( verbose ) then
    for i = 1 : k-1
      mprintf("%s\n",str(i))
    end
  end
  result = str
  //
endfunction


