// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function fvec=uncprb_getvecfcn(n,m,x,nprob)
  // Returns the function vector and the Jacobian.
  //
  // Calling Sequence
  //   fvec=uncprb_getvecfcn(n,m,x,nprob)
  //
  // Parameters
  //   n: the number of variables, i.e. the size of x
  //   m: the number of functions, i.e. the size of fvec
  //   x: a n x 1 matrix of doubles, the point where to compute f
  //   nprob: the problem number
  //   fvec: a m x 1 matrix of doubles, the vector (f1(x), f2(x), ... fm(x))^T
  //
  // Description
  // It is an interface function which calls the function func (which selects
  // appropriate test function based on nprob) with option=1 inorder to return
  // fvec , the vector.
  //
  // Examples
  //   // Get fvec and J at x0 for Rosenbrock's test case
  //   nprob = 1
  //   [n,m,x0]=uncprb_getinitf(nprob)
  //   fvec=uncprb_getvecfcn(n,m,x0,nprob)
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //   Scilab port: 2000-2004, Benoit Hamelin, Jean-Pierre Dussault
  //   Matlab port: 1994, Chaya Gurwitz, Livia Klein, Madhu Lamba
  //   Fortran 77: 1981, More, Garbow, Hillstrom

  [lhs,rhs]=argn();
  if ( rhs <> 4 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 4 are expected."), "uncprb_getvecfcn", rhs);
    error(errmsg)
  end
  //
  fvec = uncprb_getfunc(n,m,x,nprob,1);
endfunction

