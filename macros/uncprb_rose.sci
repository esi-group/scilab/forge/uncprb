// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_rose(n,m,x,option)
  // Rosenbrock function.
  //
  // Calling Sequence
  //   [fvec,J,H]=uncprb_rose(n,m,x,option)
  //
  // Parameters
  //   n: the number of variables, i.e. the size of x
  //   m: the number of functions, i.e. the size of fvec
  //   x: a n x 1 matrix of doubles, the point where to evaluate fvec and J
  //   option: option=1 set fvec only (J=[]), option=2 set J only (fvec=[]), option= 3 set both fvec and J
  //   fvec: a m x 1 matrix of doubles, the vector (f1(x), f2(x), ... fm(x))^T
  //   J: a m x n matrix of doubles, the Jacobian matrix, dfi(x)/dxj, i=1,..,m  , j=1, ..., n
  //
  // Description
  //  This function should not be made public. The user can call the uncprb_get*
  //  functions instead.
  //  <itemizedlist>
  //  <listitem>Problem no. 1</listitem>
  //  <listitem>Dimensions -> n=2, m=2</listitem>
  //  <listitem>Standard starting point -> x=[-1.2,1]'</listitem>
  //  <listitem>Minima -> f=0 at x=[1 1]'</listitem>
  //  </itemizedlist>
  //
  //  Revised on 10/22/94 by Madhu Lamba
  //
  // Examples
  //   nprob = 1
  //   [n,m,x0]=uncprb_getinitf(nprob)
  //   option = 3
  //   [fvec,J,H] = uncprb_rose(n,m,x0,option)
  //
  //   [fopt,xopt] = uncprb_getopt(nprob,n,m)
  //   option = 3
  //   [fvec,J,H] = uncprb_rose(n,m,xopt,option)
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //   Scilab port: 2000-2004, Benoit Hamelin, Jean-Pierre Dussault
  //   Matlab port: Chaya Gurwitz, Livia Klein, Madhu Lamba
  //   Fortran 77: 1981, More, Garbow, Hillstrom

  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_rose",option);
  end
  apifun_checktrue ( n==2 , msprintf(gettext("%s: Expected n==2, but got the contrary. n=%d"), "uncprb_rose" , n ))
  apifun_checktrue ( m==2 , msprintf(gettext("%s: Expected m==2, but got the contrary. m=%d"), "uncprb_rose" , m ))
  //
  fvec=[]
  J=[]
  H=[]
  //
  if ( option==1 | option==3 | option==4 ) then
    fvec = [10*(x(2)-(x(1)^2));1-x(1)]
  end
  //
  if ( option==2 | option==3 | option==4 ) then
    J = [-20*x(1),10;-1,0]
  end
  //
  if ( option==4 ) then
    hesl(1: n*(n-1)/2) = 0
    for j = 1:2: n
      hesd(j+1) = 200
      hesd(j) = 1200*x(j)**2 - 400*x(j+1) + 2
      hesl(j*(j-1)/2+j) = -400*x(j)
    end
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

