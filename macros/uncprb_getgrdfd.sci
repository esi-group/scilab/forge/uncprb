// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.


function gfd = uncprb_getgrdfd ( varargin )
  // Compute the gradient by finite differences.
  //
  // Calling Sequence
  //   gfd = uncprb_getgrdfd ( n , m , x , nprob )
  //   gfd = uncprb_getgrdfd ( n , m , x , nprob , gstep )
  //   gfd = uncprb_getgrdfd ( n , m , x , nprob , gstep , gorder )
  //
  // Parameters
  //   x: a n x 1 matrix of doubles, the point where to compute f
  //   gstep : the step to use for differences. If gstep=[], uses the default step. The default step depends on the order and the machine precision %eps.
  //   gorder the order to use for differences (1, 2 or 4). Default gorder = 2. If gorder=[], uses the default order.
  //   nprob: the problem number
  //   n: the number of variables, i.e. the size of x
  //   m: the number of functions, i.e. the size of fvec
  //   gfd: a 1 x n matrix of doubles, the gradient, df(x)/dxj, j=1, ..., n
  //
  // Description
  // Uses finite differences to compute the Jacobian matrix.
  // Does not exploit the structure to compute the difference.
  //
  // Examples
  //   // Get Jfd at x0 for Rosenbrock's test case
  //   nprob = 1
  //   [n,m,x0]=uncprb_getinitf(nprob)
  //   gfd = uncprb_getgrdfd ( n , m , x0 , nprob )
  //   // Compare with exact gradient
  //   g = uncprb_getgrdfcn (n,m,x0,nprob)
  //   norm(g-gfd)/norm(g)
  //   // Set the step
  //   gfd = uncprb_getgrdfd ( n , m , x0 , nprob , 1.e-1 )
  //   // Set the step and the order
  //   gfd = uncprb_getgrdfd ( n , m , x0 , nprob , 1.e-1 , 4 )
  //   // Set the order (use default step)
  //   gfd = uncprb_getgrdfd ( n , m , x0 , nprob , [] , 4 )
  //   // Use default step and order
  //   gfd = uncprb_getgrdfd ( n , m , x0 , nprob , [] , [] )
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO

  //
  [lhs,rhs]=argn();
  if ( rhs < 4 | rhs > 6 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 4 to 6 are expected."), "uncprb_getgrdfd", rhs);
    error(errmsg)
  end
  //
  n = varargin(1)
  m = varargin(2)
  x = varargin(3)
  nprob = varargin(4)
  if ( rhs < 5 ) then
    gstep = []
  else
    gstep = varargin(5)
  end
  if ( rhs < 6 ) then
    gorder = []
  else
    gorder = varargin(6)
  end
  //
  // Define a wrapper over the objective function for derivative
  // Caution : transpose the gradient !
  body = [
  "fout=uncprb_getobjfcn("+string(n)+","+string(m)+",x,"+string(nprob)+")"
  ];
  prot=funcprot();
  funcprot(0);
  deff("fout=objfun(x)",body);
  funcprot(prot);
  //
  if ( gstep == [] ) then
    if ( gorder == [] ) then
      //gfd = derivative(objfun,x0);
      gfd = numderivative(objfun,x0);
    else
      //gfd = derivative(objfun,x0,order=gorder );
      gfd = numderivative(objfun,x0,[],order=gorder );
    end
  else
    if ( gorder == [] ) then
      //gfd = derivative(objfun,x0,gstep);
      gfd = numderivative(objfun,x0,gstep);
    else
      //gfd = derivative(objfun,x0,gstep,gorder );
      gfd = numderivative(objfun,x0,gstep,gorder );
    end
  end
endfunction

