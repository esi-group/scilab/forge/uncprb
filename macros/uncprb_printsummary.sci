// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function success = uncprb_printsummary(name, nprob, fopt, xopt, gopt, iter, funeval, geval, ..
    rtolF, atolF, rtolX, atolX, fmt)
    // Prints a summary of an optimization.
    //
    // Calling Sequence
    //   success = uncprb_printsummary(name, nprob, fopt, xopt, gopt, iter, funeval, geval, ..
    //       rtolF, atolF, rtolX, atolX, fmt)
    //
    // Parameters
    //   name: a 1-by-1 matrix of strings, the name of the problem or the name of the solver
    //   nprob: a 1-by-1 matrix of doubles, integer value, the problem number
    //   fopt: a 1-by-1 matrix of doubles, the computed optimum F(X)
    //   xopt: a n-by-1 matrix of doubles, the computed optimum X
    //   gopt: a n-by-1 matrix of doubles, the computed optimum gradient G(X). If gopt==[], then gopt is not printed.
    //   iter: a 1-by-1 matrix of doubles, integer value, the number of iterations. If iter==[], then iter is not printed.
    //   funeval: a 1-by-1 matrix of doubles, integer value, the number of function evaluations
    //   geval: a 1-by-1 matrix of doubles, integer value, the number of gradient evaluations. If geval==[], then geval is not printed.
    //   rtolF: a 1-by-1 matrix of doubles, positive, a relative tolerance on F
    //   atolF: a 1-by-1 matrix of doubles, positive, an absolute tolerance on F
    //   rtolX: a 1-by-1 matrix of doubles, positive, a relative tolerance on X
    //   atolX: a 1-by-1 matrix of doubles, positive, an absolute tolerance on X
    //   fmt: a 1-by-1 matrix of strings, the format of the output. Available values are fmt="detailed", "line", "wiki" or "wikihead".
    //   success : a 1-by-1 matrix of booleans. success is %t if the optimization is correct both on X and on F
    //
    // Description
    // Prints a summary of an optimization.
    //
    // The success of the optimization is based on the uncprb_computestatus function.
	//
	// Depending on the value of the fmt string, the output of uncprb_printsummary changes.
    // <itemizedlist>
    // <listitem><para>
	// If fmt=="detailed", then a detailed message is produced.
    // </para></listitem>
    // <listitem><para>
    // If fmt=="line", then a one-line message is produced.
    // </para></listitem>
    // <listitem><para>
    // If fmt=="wiki", then a one-line message is produced, with Wiki formatting.
    // </para></listitem>
    // <listitem><para>
    // If fmt=="wikihead", then the first line of a wiki table is printed.
    // </para></listitem>
    // </itemizedlist>
    //
    // Examples
    //   // Make optim optimize the problem #1
    //   nprob = 1;
    //   [n,m,x0]=uncprb_getinitf(nprob)
    //   uncprb_optiminit();
	//   objfun = list(uncprb_optimfun,nprob,n,m);
    //   [fopt,xopt,gopt]=optim(objfun,x0,"gc")
    //   funeval = uncprb_optimget();
	//   geval = [];
	//   iter = [];
    //   rtolX = 1.e-4;
    //   atolX = 1.e-10;
    //   rtolF = 1.e-4;
    //   atolF = 1.e-10;
    //   //
    //   // Format "detailed"
    //   success = uncprb_printsummary("optim/UNC/gc", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
    //       rtolF, atolF, rtolX, atolX, "detailed")
    //   //
    //   // Format "line"
    //   success = uncprb_printsummary("optim/UNC/gc", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
    //       rtolF, atolF, rtolX, atolX, "line")
    //   //
    //   // Format "wiki"
    //   success = uncprb_printsummary("optim/UNC/gc", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
    //       rtolF, atolF, rtolX, atolX, "wiki")
    //
    // Authors
    //   Michael Baudin - 2011 - DIGITEO

    if ( or(fmt==["detailed" "line" "wiki"]) ) then
    [statusF, statusX] = uncprb_computestatus ( nprob, xopt, fopt, rtolF, atolF, rtolX, atolX )
    [fstar,xstar] = uncprb_getopt(nprob,n,m);
    [n,m,x0] = uncprb_getinitf(nprob);
    xopt = xopt(:)
    xstar = xstar(:)
    success = ( statusX & statusF )
    prbname = uncprb_getname(nprob)
	prbname = convstr(prbname, "u")
	end
    if ( fmt=="detailed" ) then
        mprintf("%s:\n",name);
        if ( iter <> [] ) then
            mprintf("  Iterations: %d\n", iter);
        end
        mprintf("  F eval: %d\n", funeval);
        if ( geval <> [] ) then
            mprintf("  G eval: %d\n", geval);
        end
        mprintf("  Status X: %s\n", string(statusX));
        mprintf("  Status F: %s\n", string(statusF));
        mprintf("  f(X)=%s (f*=%s)\n", string(fopt),string(fstar));
        if ( fstar <> 0 ) then
            relerr = abs(fopt-fstar)/abs(fstar)
            mprintf("  Rel. Err. on F: %s\n", string(relerr));
        end
        if ( gopt <> [] ) then
            mprintf("  ||G(X)||= %s\n",string(norm(gopt)))
        end
        if ( xstar <> [] ) then
            aerr = norm(xopt-xstar)
            mprintf("  ||X-X*||=%s\n", string(aerr));
            if ( and(xstar<> 0)  ) then
                relerr = max(abs(xopt-xstar)./abs(xstar))
                mprintf("  Rel. Err. on X: %s\n", string(relerr));
            end
        end
        aerr = abs(fopt-fstar)
        mprintf("  |f(x)-f(x*)|=%s\n", string(aerr));
    elseif ( fmt=="line" ) then
        mprintf("Problem #%3d (%10s-%3s), f*=%10s, f(x*)=%10s", ..
        nprob , prbname, string(n), string(fstar), string(fopt))
        if ( gopt <> [] ) then
            mprintf(", ||g(x*)||=%10s", string(norm(gopt)))
        end
        mprintf(", Feval=%4d", funeval)
        if ( geval<> [] ) then
            mprintf(", Geval=%4d", geval)
        end
        mprintf(", Status=%s\n", string(success))
    elseif ( fmt=="wiki" ) then
        mprintf("|| #%d || %10s || %3s || %10s || %10s ||", ..
        nprob , prbname, string(n), string(fstar), string(fopt) )
        if ( gopt<>[] ) then
            mprintf(" %10s ||", string(norm(gopt)))
        end
        mprintf(" %4d ||", funeval)
        if ( geval<>[] ) then
            mprintf(" %4d ||", geval)
        end
        mprintf(" %s ||\n", string(success))
    elseif ( fmt=="wikihead" ) then
        mprintf("|| ''''''Problem'''''' || ''''''Name'''''' || ''''''n'''''' || ''''''F*'''''' || ''''''F(X)'''''' ||")
        if ( gopt<>[] ) then
			mprintf(" ''''''norm(G(X))'''''' ||")
		end
        mprintf(" ''''''Feval'''''' ||")
        if ( geval<>[] ) then
			mprintf(" ''''''Geval'''''' ||")
		end
		mprintf(" ''''''Status'''''' ||\n")
    else
		lclmsg = "%s: Wrong value for input argument #%d"
        error(msprintf(gettext(lclmsg),"uncprb_printsummary",13))
    end
endfunction
