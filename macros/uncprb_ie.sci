// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_ie(n,m,x,option)

  // Discrete integral equation function
  // -----------------------------------
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // function [fvec,J]=ie(n,m,x,option)
  // Dimensions -> n=variable, m=n
  // Standard starting point -> x=(s(j)) where
  //                            s(j)=t(j)*(t(j)-1) where
  //                            t(j)=j*h & h=1/(n+1)
  // Minima -> f=0
  //
  // 12/4/94 by Madhu Lamba
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if ( option<1 | option>3 ) then
    msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_ie",option);
  end
  //
  apifun_checktrue ( m==n , msprintf(gettext("%s: Expected m==n, but got the contrary. m=%d, n=%d"), "uncprb_ie" , m , n ))
  //
  fvec=[]
  J=[]
  //
  if ( option==2 | option==3 ) then
    J = zeros(m,n);
  end
  h = 1/(n+1);
  for i = 1:n
    t(1,i) = i*h;
  end

  for i = 1:n

    if ( option==1 | option==3 ) then

      x(n+1) = 0
      sum1 = 0;
      for j = 1:i
        sum1 = sum1+t(j)*((x(j)+t(j)+1)^3);
      end
      sum2 = 0;
      if n>i then
        for j = i+1:n
          sum2 = sum2+(1-t(j))*((x(j)+t(j)+1)^3);
        end
      end
      fvec(i) = x(i)+h*((1-t(i))*sum1+t(i)*sum2)/2;
    end

    if ( option==2 | option==3 ) then
      for j = 1:n
        if j<i then
          J(i,j) = 3*h/2*(1-t(i))*t(j)*((x(j)+t(j)+1)^2);
        elseif j>i then
          J(i,j) = 3*h/2*t(i)*(1-t(j))*((x(j)+t(j)+1)^2);
        elseif j==i then
          J(i,i) = 1+3*h/2*(1-t(i))*t(i)*((x(i)+t(i)+1)^2);
        end
      end
    end
  end
endfunction
function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

