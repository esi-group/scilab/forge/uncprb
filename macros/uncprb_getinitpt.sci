// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [n,m,x]=uncprb_getinitpt(varargin)
  // Returns the starting point.
  //
  // Calling Sequence
  //   [n,m,x]=uncprb_getinitpt(nprob)
  //   [n,m,x]=uncprb_getinitpt(nprob,fact)
  //
  // Parameters
  //   nprob: the problem number
  //   fact: a 1 x 1 matrix of doubles, multiplier for the coordinate of x0 (default = 1)
  //   n: the number of variables, i.e. the size of x
  //   m: the number of functions, i.e. the size of fvec
  //   x: a n x 1 matrix of doubles, the (multiplied) starting point
  //
  // Description
  // This function generates the starting points for each problem
  // by calling the function initf, which sets m, n and the starting
  // point xo for the number of problem given to it. If xo is the
  // standard starting point, then x will contain fact*xo, except
  // if xo is the zero vector and fact is not unity, then all the
  // components of x will be set to fact.
  //
  // Examples
  //   // Get starting data for Rosenbrock's test case
  //   // n=2, m=2, x=[-1.2,1]'
  //   nprob = 1
  //   [n,m,x]=uncprb_getinitpt(nprob)
  //
  //   // Get starting data for Rosenbrock's test case
  //   // Multiplies x0 by 3
  //   [n,m,x]=uncprb_getinitpt(nprob,3)
  //
  //   // Get a sequence of starting points,
  //   // increasingly away from the usual one.
  //   nprob = 1
  //   alpha = 5
  //   ntries = 10
  //   fact = 1
  //   for k = 1 : ntries
  //     [n,m,x0]=uncprb_getinitpt(nprob,fact);
  //     fact = alpha * fact;
  //     disp([k x0'])
  //   end
  //
  //   // Get a sequence of starting points,
  //   // randomly in the neighbourhood of the usual one.
  //   nprob = 1
  //   ntries = 10
  //   len = 2
  //   [n,m,x0]=uncprb_getinitpt(nprob);
  //   plot ( x0(1) , x0(2) , "ro" )
  //   for k = 1 : ntries
  //     [n,m,x0]=uncprb_getinitpt(nprob);
  //     x0 = x0 + len * (2*rand(n,1)-1);
  //     disp([k x0'])
  //     plot ( x0(1) , x0(2) , "b*" )
  //   end
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //   Scilab port: 2000-2004, Benoit Hamelin, Jean-Pierre Dussault
  //   Matlab port: 1994, Chaya Gurwitz, Livia Klein, Madhu Lamba
  //   Fortran 77: 1981, More, Garbow, Hillstrom



  [lhs,rhs]=argn();
  if ( rhs < 1 | rhs > 2 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 1 or 2 are expected."), "uncprb_getinitpt", rhs);
    error(errmsg)
  end
  //
  nprob = varargin(1)
  if ( rhs < 2 ) then
    fact = 1.0
  else
    fact = varargin(2)
  end
  //
  [n,m,x] = uncprb_getinitf(nprob)
  x = fact * x
endfunction

