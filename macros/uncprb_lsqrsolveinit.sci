// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function uncprb_lsqrsolveinit()
    // Initialize the internal data for uncprb_lsqrsolve
    //
    // Calling Sequence
    //   uncprb_lsqrsolveinit()
    //
    // Parameters
    //
    //
    // Description
    // Initialize the internal data used by uncprb_lsqrsolvefun and
    // uncprb_lsqrsolvejac.
    //
    // Examples
    //   // See uncprb_lsqrsolvefun
    //
    // Authors
    //   Michael Baudin - 2011 - DIGITEO

    global __LSQRSOLVEDATA__

    __LSQRSOLVEDATA__.funeval = 0
    __LSQRSOLVEDATA__.Jeval = 0
endfunction

