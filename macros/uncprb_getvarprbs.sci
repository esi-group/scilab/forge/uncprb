// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [prb_n,prb_m] = uncprb_getvarprbs()
  // Returns the array of problems with variable n or m.
  //
  // Calling Sequence
  //   [prb_n,prb_m] = uncprb_getvarprbs()
  //
  // Parameters
  //   prb_n: a matrix of floating point integers, the problems with n variable
  //   prb_m: a matrix of floating point integers, the problems with m variable
  //
  // Description
  // For these problems, the user can set the size of the
  // problem to an arbitrary value.
  //
  // Examples
  //   // Get the matrix of variable size problems
  //   prbmat = uncprb_getvarprbs()
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO

  [lhs,rhs]=argn();
  if ( rhs <> 0 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while at most 0 are expected."), "uncprb_getvarprbs", rhs);
    error(errmsg)
  end
  //
  prb_n = [20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35]'
  prb_m = [6 11 12 16 18 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35]'
endfunction

