// Copyright (C) 2010-2011 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_bard(n,m,x,option)

    //  function [fvec,J]= bard(n,m,x,option)
    //  Bard function       [8]
    //  Dimensions  n=3,    m=15
    //  Function definition:
    //       f(x) = y(i) - [x1 + (u(i) / v(i)x2 + w(i)x3)]
    //       where u(i) = i, v(i) = 16-i, w(i) = min(u(i),v(i))
    //  Standard starting point at x= (1,1,1)
    //  Minima f=8.21487...10^(-3)   and f=17.4286 at (0.8406...,-inf,-inf)
    //

    if ( option<1 | option>3 ) then
        msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_osb2",option);
    end
    //
    fvec=[]
    J=[]
    //
    y = [0.14,0.18,0.22,0.25,0.29,0.32,0.35,0.39,0.37,0.58,0.73,0.96,1.34,2.1,4.39]';

    if ( option==2 | option==3 ) then
        J = zeros(m,n);
    end

    u = (1:m)';
    v = 16-(1:m)';
    w = min(u,v);

    if ( option==1 | option==3 ) then
        fvec = y-(x(1)+u./(v*x(2)+w*x(3)));
    end
    if ( option==2 | option==3 ) then
        i = (1:m)'
        J(i,1) = -1;
        J(i,2) = u.*v./((v*x(2)+w*x(3)).^2);
        J(i,3) = u.*w./((v*x(2)+w*x(3)).^2);
    end

endfunction
function apifun_checktrue ( var , msg )
    if ( ~var ) then
        error(msg);
    end
endfunction

