// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_froth(n,m,x,option)
  // Freudenstein and Roth function
  // ------------------------------
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // function [fvec,J]=froth(n,m,x,option)
  // Problem no. 2
  // Dimensions -> n=2, m=2
  // Standard starting point -> x=(0.5,-2)
  // Minima -> f=0 at (5,4)
  //           f=48.9842... at (11.41...,-0.8968...)
  //
  // Revised on 10/22/94 by Madhu Lamba
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if ( option<1 | option>3 ) then
    msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_froth",option);
  end

  fvec=[]
  J=[]

  if ( option==1 | option==3 ) then
    fvec = [-13+x(1)+((5-x(2))*x(2)-2)*x(2);-29+x(1)+((x(2)+1)*x(2)-14)*x(2)];
  end
  if ( option==2 | option==3 ) then
    J = [1,10*x(2)-3*(x(2)^2)-2;1,3*(x(2)^2)+2*x(2)-14];

  end
  //
endfunction
function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

