// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_vardim(n,m,x,option)

  // Variably Dimensioned function
  // -----------------------------
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // function [fvec,J]=vardim(n,m,x,option)
  // Dimensions -> n=variable, m=n+2
  // Problem no. 25
  // Standard starting point -> x=(s(j)) where
  //                            s(j)=1-(j/n)
  // Minima -> f=0 at (1,.....1)
  //
  // 11/21/94 by Madhu Lamba
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_vardim",option);
  end
  //
  apifun_checktrue ( m==n+2 , msprintf(gettext("%s: Expected m==n+2, but got the contrary. m=%d, n=%d"), "uncprb_vardim" , m , n ))
  //
  fvec=[]
  J=[]
  H=[]
  //
  if ( option==2 | option==3 | option==4 ) then
    J = zeros(m,n);
  end
  for i = 1:n
    if ( option==1 | option==3 | option==4 ) then
      fvec(i) = x(i)-1;
    end
    if ( option==2 | option==3 | option==4 ) then
      J(i,i) = 1;
    end
  end

  var_1 = 0;
  for j = 1:n
    var_1 = var_1+j*(x(j)-1);
  end
  if ( option==1 | option==3 | option==4 ) then
    fvec(n+1) = var_1
    fvec(n+2) = var_1^2
  end

  if ( option==2 | option==3 | option==4 ) then
    for j = 1:n
      J(n+1,j) = j;
      J(n+2,j) = 2*var_1*j;
    end
  end
  //
  if ( option==4 ) then
    t1 = 0
    for j = 1: n
      t1 = t1 + j*(x(j)-1)
    end
    t = 1 + 6*t1**2
    im = 0
    for j = 1: n
      hesd(j) = 2 + 2*t*j**2
      for k = 1: j-1
        im = im + 1
        hesl(im) = 2*t*j*k
      end
    end
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

