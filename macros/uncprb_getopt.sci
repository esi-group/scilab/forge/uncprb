// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000 - John Burkardt
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fopt,xopt] = uncprb_getopt(nprob,n,m)
  // Returns the name.
  //
  // Calling Sequence
  //   [fopt,xopt] = uncprb_getopt(nprob,n,m)
  //
  // Parameters
  //   nprob: a floating point integers, the problem number
  //   n: the number of variables, i.e. the size of x
  //   m: the number of functions, i.e. the size of fvec
  //   fopt : a 1 x 1 matrix of doubles, the minimum of the function
  //   xopt : a matrix of doubles, n x 1, the optimum of the function
  //
  // Description
  // Returns the optimum, according to the paper "Algo 566".
  // For each problem this corresponds to the data in the (d) section.
  // For some problems, the optimum is known only for particular values of m and n.
  // If fopt or xopt is unknown, an error is returned.
  //
  // When the paper mentions several local optimums, we return
  // the first one in the paper. When the paper only gives
  // a limited number of significant digits, so do we.
  //
  // When the paper writes +/-inf, we set +/-%inf.
  //
  // When the paper gives the function value, but not the optimum,
  // we return xopt=[]. This allows to return something,
  // instead of nothing at all.
  //
  // There are some cases where the optimum is known for several
  // particular values of n or m. This is why n and m are input
  // arguments. If it turns out that the optimum is not
  // known for this particular value of n or m, we generate
  // an error.
  //
  // We emphasize that, when xopt is unknown, there is no
  // way to check that fopt is correct, that is satisfies
  // f(xopt)=fopt and g(xopt)=0.
  //
  // All default settings provided by getinitf do correspond to
  // the parameters used here. This allows to make checkings with
  // the default settings, when possible.
  //
  // For problem #20 - Watson - the values of the approximate solutions are taken from Brent,
  // "Algorithms for Minimization with Derivatives", Prentice-Hall, 1973.
  // The values are taken from John Burkardt - "test_opt" - March 2000.
  // I improved the precision based on a full-precision optimization with optim : the
  // gradient is smaller.
  //
  // For problem #3 - Powell Badly Scaled - a manual optimization was done
  // and the results are reported to full precision. We claim
  // that the gradient is exactly zero at optimum.
  //
  // Examples
  //   // Get optimum for Rosenbrock's test case
  //   nprob = 1;
  //   [n,m,x0]=uncprb_getinitf(nprob);
  //   [fopt,xopt] = uncprb_getopt(nprob,n,m)
  //   // Check that the optimum is known is this case
  //   isknown = ( xopt <> [] )
  //
  //   // Check what is provided for problem #8
  //   nprob = 8;
  //   [n,m,x0]=uncprb_getinitf(nprob);
  //   [fopt,xopt] = uncprb_getopt(nprob,n,m)
  //   // See that fopt is known, but not xopt
  //   isknown = ( xopt <> [] )
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //   Scilab port: 2000-2004, Benoit Hamelin, Jean-Pierre Dussault
  //   Optimum for problem #20 - 2000 - John Burkardt
  //   Matlab port: 1994, Chaya Gurwitz, Livia Klein, Madhu Lamba
  //   Fortran 77: 1981, More, Garbow, Hillstrom
  //
  // References
  // "Mor� set of test functions", Kuntsevich, http://www.uni-graz.at/imawww/kuntsevich/solvopt/results/moreset.html

  [lhs,rhs]=argn();
  if ( rhs <> 3 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 3 are expected."), "uncprb_getname", rhs);
    error(errmsg)
  end
  //
  if ( nprob==1 ) then
    fopt = 0
    xopt = [1 1]'
  elseif ( nprob==2 ) then
    fopt = 0
    xopt = [5 4]'
	// There is also a local optimum at :
	// f=48.98425 at the point [11.4125; -0.89685].
  elseif ( nprob==3 ) then
    fopt = 0
    xopt = [1.098159329699795501D-05  9.106146739866707307D+00]'
  elseif ( nprob==4 ) then
    fopt = 0
    xopt = [1.e6 2.e-6]'
  elseif ( nprob==5 ) then
    fopt = 0
    xopt = [3 0.5]'
  elseif ( nprob==6 ) then
    apifun_checktrue ( m==10 , msprintf(gettext("%s: Expected m==10, but got the contrary. m=%d"), "uncprb_getopt" , m ))
    fopt = 124.362
    xopt = [0.257825; 0.257825]
	// There is also a local optimum at:
	//  f=259.58019078047 on the plane x1=0.33148432
  elseif ( nprob==7 ) then
    fopt = 0
    xopt = [1 0 0]'
  elseif ( nprob==8 ) then
    fopt = 8.21487e-3
    xopt = [.0824106 1.13304 2.3437]
  elseif ( nprob==9 ) then
    fopt = 1.127932769618891408D-08
    xopt = [3.989561378387566637D-01    1.000019084487805632D+00  0]
  elseif ( nprob==10 ) then
    fopt = 87.9458
    xopt = []
  elseif ( nprob==11 ) then
    fopt = 0
    xopt = [50 25 1.5]'
	// There is also a local optimum at:
	// f=0.038 at x=[99.89537833835886; 60.61453903025014; 9.16124389236433]
	// and another one at:
	// f=0.0380 at x=[201.662589489426; 60.616331504682; 10.224891158489]
  elseif ( nprob==12 ) then
    fopt = 0
    xopt = [1 10 1]'
  elseif ( nprob==13 ) then
    fopt = 0
    xopt = zeros(n,1)
  elseif ( nprob==14 ) then
    fopt = 0
    xopt = [1 1 1 1]'
  elseif ( nprob==15 ) then
    fopt = 3.07505e-4
    xopt = [0.192807; 0.191282; 0.123056; 0.136062]
	// There is also the local minimum f=0.00179453906640
  elseif ( nprob==16 ) then
    apifun_checktrue ( m==20 , msprintf(gettext("%s: Expected m==20, but got the contrary. m=%d"), "uncprb_getopt" , m ))
    fopt = 85822.2
    xopt = []
  elseif ( nprob==17 ) then
    fopt = 5.464894697482391051D-05
    xopt = [3.754100520989722689D-01 1.935846912323193170D+00 -1.464687136223910224D+00 1.286753463903115950D-02 2.212269966307561786D-02]
	// There is a FALSE minimum f=1.10603621875 on a plateau
  elseif ( nprob==18 ) then
    apifun_checktrue ( m==13 , msprintf(gettext("%s: Expected m==13, but got the contrary. m=%d"), "uncprb_getopt" , m ))
    fopt = 0
    xopt = [1; 10; 1; 5; 4; 3]
	// There is an approximate saddle point (||G(X)||= 3.185D-09) :
	//   f=0.00565565 at x=[1.711416; 17.6831975; 1.16314365; 5.18656; 1.711416; 1.16314365]
	// and a local minimum :
	//   f = 0.30636677262479 on the plane {x1=1.22755594752403; x2>>0; x3=0.83270306333466; x4<<0; x5=x1; x6=x3}
  elseif ( nprob==19 ) then
    fopt = 4.01377e-2
    xopt = [1.30997715348596; 0.431553792599067; 0.633661699574371; 0.599430531571756; 0.754183220153148; 0.9042885798778312889112; 1.3658118352548440643801; 4.8236988172651047435124; 2.3986848661381050540342;  4.5688745976651192748363; 5.6753414705795801609156]
	// Other local minimum :
	// f=1.78981358688109 at x1=0.91703842587974 and x5=0.13275478535299
	// f=26.305657 at x1=1.3660000675 and x5=0.6.
	// In the both cases, the other nine variables may take various values.
  elseif ( nprob==20 ) then
    apifun_checktrue ( n==6 | n==9 | n==12, msprintf(gettext("%s: Expected n==9 | n==12, but got the contrary. n=%d"), "uncprb_getopt" , n ))
    if ( n==6 ) then
      fopt = 2.28767e-3
      xopt = [-0.015725D+00, 1.012435D+00, -0.232992D+00, 1.260430D+00, -1.513729D+00, 0.992996D+00]'
    elseif ( n==9 ) then
      fopt = 1.39976e-6
      xopt = [-1.530703669113499455D-05 9.997897039319447732D-01 1.476396369401976848D-02  1.463423282932473646D-01 1.000821103030963277D+00 -2.617731140577795657D+00 4.104403164548480198D+00 -3.143612278598149512D+00 1.052626408019991633D+00]'
    elseif ( n==12 ) then
      fopt = 4.72238e-10
      xopt = []
    end
  elseif ( nprob==21 ) then
    fopt = 0
    xopt = ones(n,1)
  elseif ( nprob==22 ) then
    fopt = 0
    xopt = zeros(n,1)
  elseif ( nprob==23 ) then
    apifun_checktrue ( n==4 | n==10, msprintf(gettext("%s: Expected n==4 | n==10, but got the contrary. n=%d"), "uncprb_getopt" , n ))
    if ( n==4 ) then
      fopt = 2.24997e-5
      xopt = []
    elseif ( n==10 ) then
      fopt = 7.08765e-5
      xopt = []
    end
  elseif ( nprob==24 ) then
    apifun_checktrue ( n==4 | n==10, msprintf(gettext("%s: Expected n==4 | n==10, but got the contrary. n=%d"), "uncprb_getopt" , n ))
    if ( n==4 ) then
      fopt = 9.37629e-6
      xopt = []
    elseif ( n==10 ) then
      fopt = 2.93660e-4
      xopt = []
    end
  elseif ( nprob==25 ) then
    fopt = 0
    xopt = ones(n,1)
  elseif ( nprob==26 ) then
    fopt = 0
    xopt = [0.0429645656464827; 0.043976286943042; 0.0450933996565844; 0.0463389157816542; 0.0477443839560646; 0.0493547352444078; 0.0512373449259557; 0.195209463715277; 0.164977664720328; 0.0601485854398078]
	// Other local minimum :
	// f=2.79506e-5 at x=[ 0.0551509; 0.0568408; 0.0587639; 0.0609906; 0.0636263; 0.0668432; 0.208162; 0.164363; 0.0850068; 0.0914314].
  elseif ( nprob==27 ) then
    fopt = 0
    xopt = ones(n,1)
  elseif ( nprob==28 ) then
    fopt = 0
    xopt = []
  elseif ( nprob==29 ) then
    fopt = 0
    xopt = []
  elseif ( nprob==30 ) then
    fopt = 0
    xopt = []
	// Other local minima:
	// f=1.36025590473840 at x=[0.89571637066795; 1.43694322050304; -0.08787533663029; -0.51565133590034; -0.56252955081589; -0.42678887777818; -0.03488113362957; 0.73773668071208; 1.31192604432832; 0.23349654053705],
	// f=1.02865203567795 at x=[1.57293556968858; 0.37536914085555; 0.16829821000091; 0.60512754208275; 1.05196056858588; 0.58619628989910; 0.42021181170450; 0.76713494858584; 1.17153397159970; 0.26636104677652],
	// f=1.05122618838356 at x=[1.76423409318896; 0.03091840088999; -0.32488644889735; -0.06855209483793; 0.70236806246591; 1.49772106403168; -0.06585213920739; -0.50614689954560; -0.54944142333092; -0.40108999846985],
	// f=1.05122618838356 at x=[1.73905838100313;0.08286257408083;-0.24655906721381;0.04553098676945; 0.74854483640949;1.17092063399475;0.39373320226816;0.28526714046312; 0.79188653014133;1.31220034435141],
	// f=0.71260601731262 at x=[1.83141075408277; -0.10695150598288; -0.58756136817458; -0.67305617728853; -0.66862208919252; -0.61247812756592; -0.45433311772938; -0.05529093386451; 0.75788912482266; 1.41326822790186] and
	// f=0.39737346895853 at x=[1.82910567383287; -0.10197809329322; -0.57788049656156; -0.64962654169425; -0.60679005996046; -0.44981389867908; -0.05474342424961; 0.72306839424618; 1.31937148707469; 0.23503639006362]
  elseif ( nprob==31 ) then
    fopt = 0
    xopt = []
	// Other local minima:
	// f=3.05727843 at x=[0.210713858177301; 0.260976685957722; 0.227947672085427; 0.197025588320616; 0.203229694083309; 0.221915978634404; 0.16281223895881; 0.0483025286174326; -0.0793565823288942; -0.153125186088696] and
	// f=2.68021992072616 at x=[0.21717225508353; 0.30646717454530; 0.35678613574215; 0.44839494272506; 0.57763800138617; 0.71310355796924; 0.82395652593316; 0.92190937955671; 0.99699281917885; 0.96346230718557].
  elseif ( nprob==32 ) then
    fopt = m-n
    xopt = -ones(n,1)
  elseif ( nprob==33 ) then
    fopt = ( m*(m-1) ) / ( 2*(2*m+1) )
    xopt = []
  elseif ( nprob==34 ) then
    fopt = (m^2+3*m-6)/(2*(2*m-3))
    xopt = []
  elseif ( nprob==35 ) then
    apifun_checktrue ( m==n , msprintf(gettext("%s: Expected m==n, but got the contrary. n=%d, m=%d"), "uncprb_getopt" , n , m ))
    apifun_checktrue ( n==8 | n==10, msprintf(gettext("%s: Expected n==8 | n==10, but got the contrary. n=%d"), "uncprb_getopt" , n ))
    if ( n==4 ) then
      fopt = 3.51687e-3
      xopt = []
    elseif ( n==10 ) then
      fopt = 6.50395e-3
      xopt = []
    end
  else
    error(msprintf("%s: The input argument nprob is %d but should be between 1 and 35.\n","uncprb_getname",nprob))
  end
  xopt = xopt(:)
endfunction
function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

