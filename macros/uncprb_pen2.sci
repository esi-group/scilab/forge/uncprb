// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_pen2(n,m,x,option)

  // Penalty II  function
  // -------------------
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // function [fvec,J]=pen2(n,m,x,option)
  // Dimensions -> n=variable, m=2*n
  // Problem no. 24
  // Standard starting point -> x=(1/2,......,1/2)
  // Minima -> f=9.37629...10^(-6)  if n=4
  //           f=2.93660...10^(-4)  if n=10
  //
  // 11/21/94 by Madhu Lamba
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // TODO : vectorize


  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_pen2",option);
  end
  //
  apifun_checktrue ( m==2*n , msprintf(gettext("%s: Expected m==2*n, but got the contrary. m=%d, n=%d"), "uncprb_pen2" , m , n ))
  //
  fvec=[]
  J=[]
  H=[]
  //
  if ( option==2 | option==3 | option==4 ) then
    J = zeros(m,n);
  end
  if ( option==1 | option==3 | option==4 ) then
    fvec(1) = x(1)-0.2;
  end

  if ( option==2 | option==3 | option==4 ) then
    J(1,1) = 1;
  end

  if n>=2 then

    for i = 2:n
      y(1,i) = exp(i/10)+exp((i-1)/10);

      if ( option==1 | option==3 | option==4 ) then
        fvec(i) = sqrt(0.00001)*(exp(x(i)/10)+exp(x(i-1)/10)-y(i))
      end

      if ( option==2 | option==3 | option==4 ) then
        J(i,i) = sqrt(0.00001)*exp(x(i)/10)*(1/10);
        J(i,i-1) = sqrt(0.00001)*exp(x(i-1)/10)*(1/10);
      end
    end

    for i = n+1:2*n-1

      if ( option==1 | option==3 | option==4 ) then
        fvec(i) = sqrt(0.00001)*(exp(x(i-n+1)/10)-exp(-1/10))
      end

      if ( option==2 | option==3 | option==4 ) then
        J(i,i-n+1) = sqrt(0.00001)*exp(x(i-n+1)/10)*(1/10);
      end
    end
  end

  if ( option==1 | option==3 | option==4 ) then

    total = 0;
    for j = 1:n
      total = total+(n-j+1)*(x(j)^2);
    end

    fvec(2*n) = total-1
  end
  if ( option==2 | option==3 | option==4 ) then
    for j = 1:n
      J(m,j) = (n-j+1)*2*x(j);
    end
  end
  //
  if ( option==4 ) then
    t1 = -1
    for j = 1: n
      t1 = t1 + (n-j+1)*x(j)**2
    end
    d1 = exp(0.1)
    d2 = 1
    th = 4*t1
    im = 0
    for j = 1: n
      hesd(j) = 8*((n-j+1)*x(j))**2 + (n-j+1)*th
      s1 = exp(x(j)/10)
      if (j > 1) then
        s3 = s1 + s2 - d2*(d1 + 1)
        hesd(j) = hesd(j) + 1.0D-5*s1*(s3 + s1 - 1/d1 + 2*s1)/50
        hesd(j-1) = hesd(j-1) + 1.0D-5*s2*(s2+s3)/50
        for k = 1: j-1
          im = im + 1
          t1 = exp((k)/10)
          hesl(im) = 8*(n-j+1)*(n-k+1)*x(j)*x(k)
        end
        hesl(im) = hesl(im) + 1.0D-5*s1*s2/50
      end
      s2 = s1
      d2 = d1*d2
    end
    hesd(1) = hesd(1) + 2
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

