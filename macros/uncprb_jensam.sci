// Copyright (C) 2010-2011 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_jensam(n,m,x,option)

    // function [fvec, J]= jensam(n,m,x,option)
    // Jenrich and Sampson function [6]
    // Dimensions n=2,   m>=n
    // Function definition
    //               f(x)=2+2i-(exp[ix1] + exp[ix2])
    // Standard starting point x=(0.3,0.4)
    // minima of f=124.362 at x1=x2=0.2578 for m=10
    //

    if ( option<1 | option>3 ) then
        msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_jensam",option);
    end
    //
    apifun_checktrue ( m>=n , msprintf(gettext("%s: Expected m>=n, but got the contrary. m=%d, n=%d"), "uncprb_jensam" , m , n ))
    //
    fvec=[]
    J=[]
    //
    if ( option==1 | option==3 ) then
        i = (1:m)'
        fvec = 2+2*i-(exp(i*x(1))+exp(i*x(2)));
    end

    if ( option==2 | option==3 ) then
        i = (1:m)'
        J(:,1) = -i .* exp(i*x(1))
        J(:,2) = -i .* exp(i*x(2))
    end
endfunction
function apifun_checktrue ( var , msg )
    if ( ~var ) then
        error(msg);
    end
endfunction

