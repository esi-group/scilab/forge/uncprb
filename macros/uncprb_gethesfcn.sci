// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
//
// This file must be used under the terms of the GNU LGPL license.

function H = uncprb_gethesfcn(n,m,x,nprob)
  // Returns the function value.
  //
  // Calling Sequence
  //   H=uncprb_gethesfcn(n,m,x,nprob)
  //
  // Parameters
  //   n: the number of variables, i.e. the size of x
  //   m: the number of functions, i.e. the size of fvec
  //   x: a n x 1 matrix of doubles, the point where to compute f
  //   nprob: the problem number
  //   H: a nxn matrix of doubles, the Hessian matrix
  //
  // Description
  // It is an interface function which calls the function func(which selects
  // appropriate test fuction based on nprob) to return H.
  // The Hessian matrix is not available for all problems.
  // If the Hessian matrix is not available, an error is generated.
  //
  // Examples
  //   // Get Hessian at x0 for Rosenbrock's test case
  //   nprob = 1
  //   [n,m,x0]=uncprb_getinitf(nprob)
  //   H = uncprb_gethesfcn(n,m,x0,nprob)
  //
  //   // Get Hessian at xopt for Rosenbrock's test case
  //   nprob = 1
  //   [n,m,x0]=uncprb_getinitf(nprob)
  //   [fopt,xopt] = uncprb_getopt(nprob,n,m)
  //   H = uncprb_gethesfcn(n,m,xopt,nprob)
  //   // See the eigenvalues, the conditionning
  //   spec(H)
  //   cond(H)
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //   Fortran 77: 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick

  [lhs,rhs]=argn();
  if ( rhs <> 4 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 4 are expected."), "uncprb_gethesfcn", rhs);
    error(errmsg)
  end
  //
  [fvec,J,H] = uncprb_getfunc(n,m,x,nprob,4)
endfunction

