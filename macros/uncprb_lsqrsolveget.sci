// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function [funeval,Jeval] = uncprb_lsqrsolveget()
    // Get the fields of the internal data for uncprb_lsqrsolvefun.
    //
    // Calling Sequence
    //   [funeval,Jeval] = uncprb_lsqrsolveget()
    //
    // Parameters
    //   funeval: a 1-by-1 matrix of doubles, integer value, the number of function evaluations since the previous call to uncprb_optiminit()
    //   Jeval: a 1-by-1 matrix of doubles, integer value, the number of Jacobian evaluations since the previous call to uncprb_optiminit()
    //
    // Description
    // Get the internal data structure used by uncprb_lsqrsolve and
    // uncprb_lsqrsolvejac.
    //
    // The values returned by the uncprb_optimget function depends on the
    // number of calls to the uncprb_lsqrsolvefun and uncprb_lsqrsolvejac
    // function.
    // <itemizedlist>
    // <listitem><para>
    // When the uncprb_lsqrsolvefun function is called, then funeval is increased.
    // </para></listitem>
    // <listitem><para>
    // When the uncprb_lsqrsolvejac function is called, then Jeval is increased.
    // </para></listitem>
    // </itemizedlist>
    //
    // Examples
    //   // See uncprb_lsqrsolvejac
    //
    // Authors
    //   Michael Baudin - 2011 - DIGITEO

    global __LSQRSOLVEDATA__

    funeval = __LSQRSOLVEDATA__.funeval
    Jeval = __LSQRSOLVEDATA__.Jeval
endfunction

