// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.


function Hfd = uncprb_gethesfd ( varargin )
  // Compute the Hessian by finite differences.
  //
  // Calling Sequence
  //   Hfd = uncprb_gethesfd ( n , m , x , nprob )
  //   Hfd = uncprb_gethesfd ( n , m , x , nprob , Hstep )
  //   Hfd = uncprb_gethesfd ( n , m , x , nprob , Hstep , Horder )
  //
  // Parameters
  //   x: a n x 1 matrix of doubles, the point where to compute f
  //   Hstep : the step to use for differences. If Hstep=[], uses the default step. The default step depends on the order and the machine precision %eps.
  //   Horder : the order to use for differences (1, 2 or 4). Default Horder=2. If Horder=[], uses the default order.
  //   nprob: the problem number
  //   n: the number of variables, i.e. the size of x
  //   m: the number of functions, i.e. the size of fvec
  //   gfd: a n x 1 matrix of doubles, the gradient, df(x)/dxj, j=1, ..., n
  //
  // Description
  // Uses finite differences to compute the Hessian matrix.
  // Does not exploit the structure to compute the difference.
  //
  // Examples
  //   // Get Hfd at x0 for Rosenbrock's test case
  //   nprob = 1
  //   [n,m,x0]=uncprb_getinitf(nprob)
  //   Hfd = uncprb_gethesfd ( n , m , x0 , nprob )
  //   // Compare with exact Hessian
  //   H = uncprb_gethesfcn(n,m,x0,nprob)
  //   norm(H-Hfd)/norm(H)
  //   // Set the step
  //   Hfd = uncprb_gethesfd ( n , m , x0 , nprob , 1.e-1 , [] )
  //   // Set the step and the order
  //   Hfd = uncprb_gethesfd ( n , m , x0 , nprob , 1.e-1 , 4 )
  //   // Set the order (use default step)
  //   Hfd = uncprb_gethesfd ( n , m , x0 , nprob , [] , 4 )
  //   // Use default step and default order
  //   Hfd = uncprb_gethesfd ( n , m , x0 , nprob , [] , [] )
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO

  //
  [lhs,rhs]=argn();
  if ( rhs < 4 | rhs > 6 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 4 to 6 are expected."), "uncprb_gethesfd", rhs);
    error(errmsg)
  end
  //
  n = varargin(1)
  m = varargin(2)
  x = varargin(3)
  nprob = varargin(4)
  if ( rhs < 5 ) then
    Hstep = []
  else
    Hstep = varargin(5)
  end
  if ( rhs < 6 ) then
    Horder = []
  else
    Horder = varargin(6)
  end
  //
  body = [
  "fout=uncprb_getobjfcn("+string(n)+","+string(m)+",x,"+string(nprob)+")"
  ];
  prot=funcprot();
  funcprot(0);
  deff("fout=objfun(x)",body);
  funcprot(prot);
  //
  if ( Hstep == [] ) then
    if ( Horder == [] ) then
      //[gfd,Hfd] = derivative(objfun,x, H_form="blockmat");
     [gfd,Hfd] = numderivative( objfun , x , [] , [] , H_form="blockmat" );
    else
      //[gfd,Hfd] = derivative(objfun,x, order=Horder ,H_form="blockmat");
     [gfd,Hfd] = numderivative( objfun , x , [] , order=Horder ,H_form="blockmat" );
    end
  else
    if ( Horder == [] ) then
      //[gfd,Hfd] = derivative(objfun,x, Hstep, H_form="blockmat");
     [gfd,Hfd] = numderivative( objfun , x , Hstep , [] , H_form="blockmat" );
    else
      //[gfd,Hfd] = derivative(objfun,x, Hstep, Horder ,H_form="blockmat");
     [gfd,Hfd] = numderivative( objfun , x , Hstep , Horder , H_form="blockmat");
    end
  end
endfunction

