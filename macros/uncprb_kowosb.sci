// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_kowosb(n,m,x,option)


  // ***********************************************************
  // Function [fvec, J]=kowosb(n,m,x,option)
  // Kowalik and Osborne function    [15]
  // Dimensions:     n=4   m=11
  // Function Definition:
  //       f(x)= y(i) - [x1(u^2 +u*x2) / (u^2 + u*x3 + x4)]
  // Standard starting point: (0.25,0.39,0.415,0.39)
  // Minima of f= 3.07505...10^-4 and
  //           f= 1.02734...10^-3  at (inf,-14.07...,-inf,-inf)
  //
  // Coded in Matlab   October 1994        PLK
  // **********************************************************

  if ( option<1 | option>3 ) then
    msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_kowosb",option);
  end
  //
  fvec=[]
  J=[]
  //
  y = [0.1957,0.1947,0.1735,0.16,0.0844,0.0627;0.0456,0.0342,0.0323,0.0235,0.0246,0]';
  u = [4,2,1,0.5,0.25,0.167;0.125,0.1,0.0833,0.0714,0.0625,0]';
  for i = 1:m
    c1 = u(i)^2+u(i)*x(2);
    c2 = u(i)^2+u(i)*x(3)+x(4);

    if ( option==1 | option==3 ) then
      fvec(i) = y(i)-x(1)*c1/c2;
    end

    if ( option==2 | option==3 ) then
      J(i,1) = -c1/c2;
      J(i,2) = (-x(1)*u(i))/c2;
      J(i,3) = x(1)*c1*(c2^(-2))*u(i);
      J(i,4) = x(1)*c1*(c2^(-2));
    end
  end
endfunction
function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

