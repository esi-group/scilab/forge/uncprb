// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function H = uncprb_hesdl2H ( hesd , hesl )
  // Reconstitute the Hessian matrix H from hesd and hesl
  // hesd : a column matrix, the diagonal part of the Hessian
  // hesl : a column matrix, the lower part terms, row by row.

  // TODO : find better, if possible.
  H = diag(hesd)
  n = size(hesd,"*")
  kc = 1
  k1 = 1
  k2 = 1
  for i = 2:n
    H ( i , 1:i-1 ) = hesl(k1:k2)'
    H ( 1:i-1 , i ) = hesl(k1:k2)
    kc = kc + 1
    k1 = k2 + 1
    k2 = k1 + kc - 1
  end
endfunction

