// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_osb1(n,m,x,option)

  // function [fvec,J] = osb1(n,m,x,option)
  //  Osborne 1 function   [17]
  // Dimensions: n=5 , m=33
  // Function Definition:
  //       f(x)=y(i)-(x1+x2*exp[-t(i)x4]+x3*exp[-t(i)x5])
  //       where t(i)=10(i-1)
  // Standard starting point: (0.5,1.5,-1,0.01,0.02)
  // Minima of f=5.46489...10^(-5)
  //           at (.3754,1.9358,-1.4647,0.01287,0.02212)
  //
  // Revised  11/94                PLK
  // *******************************************************

  if ( option<1 | option>3 ) then
    msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_osb2",option);
  end
  //
  fvec=[]
  J=[]
  //
  x1 = x(1);
  x2 = x(2);
  x3 = x(3);
  x4 = x(4);
  x5 = x(5);
  y = zeros(1,33)

  y(1,1) = 0.844
  y(1,2) = 0.908
  y(1,3) = 0.932
  y(1,4) = 0.936
  y(1,5) = 0.925
  y(1,6) = 0.908
  y(1,7) = 0.881
  y(1,8) = 0.85
  y(1,9) = 0.818
  y(1,10) = 0.784
  y(1,11) = 0.751
  y(1,12) = 0.718
  y(1,13) = 0.685
  y(1,14) = 0.658
  y(1,15) = 0.628
  y(1,16) = 0.603
  y(1,17) = 0.58
  y(1,18) = 0.558
  y(1,19) = 0.538
  y(1,20) = 0.522
  y(1,21) = 0.506
  y(1,22) = 0.49
  y(1,23) = 0.478
  y(1,24) = 0.467
  y(1,25) = 0.457
  y(1,26) = 0.448
  y(1,27) = 0.438
  y(1,28) = 0.431
  y(1,29) = 0.424
  y(1,30) = 0.42
  y(1,31) = 0.414
  y(1,32) = 0.411
  y(1,33) = 0.406
  y = y';

  im1 = 0;
  for i = 1:m
    ti = im1*10;
    e4 = exp(-ti*x4);
    e5 = exp(-ti*x5);
    t2 = x2*e4;
    t3 = x3*e5;
    if ( option==1 | option==3 ) then
      fvec(i) = x1+t2+t3-y(i);
    end
    if ( option==2 | option==3 ) then
      J(i,1) = 1;
      J(i,2) = e4;
      J(i,3) = e5;
      J(i,4) = -ti*t2;
      J(i,5) = -ti*t3;
    end
    im1 = i;
  end
endfunction
function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

