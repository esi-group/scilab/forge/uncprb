// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_singx(n,m,x,option)

  // Extended Powell Singular function
  // ---------------------------------
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // function [fvec,J]=singx(n,m,x,option)
  // Dimensions -> n=variable but a multiple of 4, m=n
  // Problem no. 22
  // Standard starting point -> x=(s(j)) where
  //                            s(4*j-3)=3,
  //                            s(4*j-2)=-1,
  //                            s(4*j-1)=0,
  //                            s(4*j)=1
  // Minima -> f=0 at the origin.
  //
  // 11/21/94 by Madhu Lamba
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_singx",option);
  end
  //
  apifun_checktrue ( m==n , msprintf(gettext("%s: Expected m==n, but got the contrary. m=%d, n=%d"), "uncprb_singx" , m , n ))
  apifun_checktrue ( pmodulo(n,4)==0 , msprintf(gettext("%s: Expected pmodulo(n,4)==0, but got the contrary. n=%d"), "uncprb_singx" , n ))
  //
  fvec=[];
  J=[];
  H=[]
  //
  if ( option==2 | option==3 | option==4 ) then
    J = zeros(m,n);
  end
  for i = 1:m/4

    if ( option==1 | option==3 | option==4 ) then
      fvec(4*i-3) = x(4*i-3)+10*x(4*i-2);

      fvec(4*i-2) = sqrt(5)*(x(4*i-1)-x(4*i))

      fvec(4*i-1) = (x(4*i-2)-2*x(4*i-1))^2

      fvec(4*i) = sqrt(10)*((x(4*i-3)-x(4*i))^2)
    end

    if ( option==2 | option==3 | option==4 ) then
      J(4*i-3,4*i-3) = 1;
      J(4*i-3,4*i-2) = 10;
      J(4*i-2,4*i-1) = sqrt(5);
      J(4*i-2,4*i) = -sqrt(5);
      J(4*i-1,4*i-2) = 2*x(4*i-2)-4*x(4*i-1);
      J(4*i-1,4*i-1) = 8*x(4*i-1)-4*x(4*i-2);
      J(4*i,4*i-3) = 2*sqrt(10)*(x(4*i-3)-x(4*i));
      J(4*i,4*i) = 2*sqrt(10)*(x(4*i)-x(4*i-3));
    end
  end
  //
  if ( option==4 ) then
    hesl(1: n*(n-1)/2) = 0
    for j = 1:4: n
      t2 = x(j+1) - 2*x(j+2)
      t3 = x(j) - x(j+3)
      s1 = 12 * t2**2
      s2 = 120 * t3**2
      hesd(j) = 2 + s2
      hesd(j+1) = 200 + s1
      hesd(j+2) = 10 + 4*s1
      hesd(j+3) = 10 + s2
      hesl(j*(j-1)/2+j) = 20
      hesl((j+1)*j/2+j) = 0
      hesl((j+1)*j/2+j+1) = -2*s1
      hesl((j+2)*(j+1)/2+j) = -s2
      hesl((j+2)*(j+1)/2+j+1) = 0
      hesl((j+2)*(j+1)/2+j+2) = -10
    end
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

