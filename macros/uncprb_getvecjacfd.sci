// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.


function Jfd = uncprb_getvecjacfd ( varargin )
  // Compute the Jacobian by finite differences
  //
  // Calling Sequence
  //   Jfd = uncprb_getvecjacfd ( n , m , x , nprob )
  //   Jfd = uncprb_getvecjacfd ( n , m , x , nprob , Jstep )
  //   Jfd = uncprb_getvecjacfd ( n , m , x , nprob , Jstep , Jorder )
  //
  // Parameters
  //   x: a n x 1 matrix of doubles, the point where to compute f
  //   Jstep : the step to use for differences. If Jstep=[], uses the default step. The default step depends on the order and the machine precision %eps.
  //   Jorder: the order to use for differences (1, 2 or 4). Default Jorder = 2. If Jorder=[], uses the default order.
  //   nprob: the problem number
  //   n: the number of variables, i.e. the size of x
  //   m: the number of functions, i.e. the size of fvec
  //   Jfd: a m x n matrix of doubles, the Jacobian matrix, dfi(x)/dxj, i=1,..,m  , j=1, ..., n
  //
  // Description
  // Uses finite differences to compute the Jacobian matrix.
  //
  // Examples
  //   // Get Jfd at x0 for Rosenbrock's test case
  //   nprob = 1
  //   [n,m,x0]=uncprb_getinitf(nprob)
  //   Jfd = uncprb_getvecjacfd ( n , m , x0 , nprob , [] , [] )
  //   // Compare with exact Jacobian
  //   J=uncprb_getvecjac(n,m,x0,nprob)
  //   norm(J-Jfd)/norm(J)
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO

  //
  [lhs,rhs]=argn();
  if ( rhs < 4 | rhs > 6 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 4 to 6 are expected."), "uncprb_getvecjacfd", rhs);
    error(errmsg)
  end
  //
  n = varargin(1)
  m = varargin(2)
  x = varargin(3)
  nprob = varargin(4)
  if ( rhs < 5 ) then
    Jstep = []
  else
    Jstep = varargin(5)
  end
  if ( rhs < 6 ) then
    Jorder = []
  else
    Jorder = varargin(6)
  end
  //
  // Define a wrapper over the objective function for derivative
  // Caution : transpose the gradient !
  body = [
  "fvec=uncprb_getvecfcn("+string(n)+","+string(m)+",x,"+string(nprob)+")"
  ];
  prot=funcprot();
  funcprot(0);
  deff("fvec=objfunvec(x)",body);
  funcprot(prot);
  //
  if ( Jstep == [] ) then
    if ( Jorder == [] ) then
//      Jfd = derivative(objfunvec,x);
      Jfd = numderivative(objfunvec,x);
    else
//      Jfd = derivative(objfunvec,x,order=Jorder );
      Jfd = numderivative(objfunvec,x,[],order=Jorder );
    end
  else
    if ( Jorder == [] ) then
//      Jfd = derivative(objfunvec,x,Jstep);
      Jfd = numderivative(objfunvec,x,Jstep);
    else
//      Jfd = derivative(objfunvec,x,Jstep,Jorder );
      Jfd = numderivative(objfunvec,x,Jstep,Jorder );
    end
  end
endfunction

