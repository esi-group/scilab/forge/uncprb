// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_bv(n,m,x,option)
  // Discrete boundary value function
  // --------------------------------
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // function [fvec,J]=bv(n,m,x,option)
  // Dimensions -> n=variable, m=n
  // Standard starting point -> x=(s(j)) where
  //                            s(j)=t(j)*(t(j)-1) where
  //                            t(j)=j*h & h=1/(n+1)
  // Minima -> f=0
  //
  // 12/4/94 by Madhu Lamba
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if ( option<1 | option>3 ) then
    msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_bv",option);
  end
  //
  apifun_checktrue ( m==n , msprintf(gettext("%s: Expected m==n, but got the contrary. m=%d, n=%d"), "uncprb_bv" , m , n ))
  //
  fvec=[]
  J=[]
  //
  if ( option==2 | option==3 ) then
    J = zeros(m,n);
  end
  h = 1/(n+1);
  for i = 1:n
    t(1,i) = i*h;

    if ( option==1 | option==3 ) then

      x(n+1) = 0
      if i==1 then
        fvec(i) = 2*x(i)-x(i+1)+(h^2)*((x(i)+t(i)+1)^3)/2;
      elseif i==n then
        fvec(i) = 2*x(i)-x(i-1)+(h^2)*((x(i)+t(i)+1)^3)/2;
      else
        fvec(i) = 2*x(i)-x(i-1)-x(i+1)+(h^2)*((x(i)+t(i)+1)^3)/2;
      end
    end

    if ( option==2 | option==3 ) then
      J(i,i) = 2+(h^2)/2*3*((x(i)+t(i)+1)^2);
      if i<n then
        J(i,i+1) = -1;
        J(i+1,i) = -1;
      end
    end
  end
endfunction
function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

