// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function name = uncprb_getname(varargin)
  // Returns the name.
  //
  // Calling Sequence
  //   name = uncprb_getname(prbmat)
  //   name = uncprb_getname(prbmat,longflag)
  //
  // Parameters
  //   prbmat: a matrix of floating point integer, the problem number
  //   longflag : a boolean, if true returns the "long name" - human readable - of the test case. If false, returns the short name. (default = %f)
  //   name: a string, the name of the problem
  //
  // Description
  // Selects the appropriate function based on nprob.
  //
  // Examples
  //   // Get the name for Rosenbrock's test case
  //   nprob = 1
  //   name = uncprb_getname(nprob)
  //
  //   // Get all names at once
  //   nprobmax = uncprb_getproblems();
  //   namemat = uncprb_getname(1:nprobmax);
  //
  //   // Search for number of the "almost" problem
  //   nprobmax = uncprb_getproblems();
  //   namemat = uncprb_getname(1:nprobmax);
  //   nprob = find(namemat=="almost")
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //   Scilab port: 2000-2004, Benoit Hamelin, Jean-Pierre Dussault
  //   Matlab port: 1994, Chaya Gurwitz, Livia Klein, Madhu Lamba
  //   Fortran 77: 1981, More, Garbow, Hillstrom

  [lhs,rhs]=argn();
  if ( rhs > 2 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while at most 2 are expected."), "uncprb_getname", rhs);
    error(errmsg)
  end
  //
  prbmat = varargin(1)
  if ( size(prbmat,"r") <> 1 ) then
    errmsg = msprintf(gettext("%s: Unexpected number rows in prbmat : %d provided while 1 is expected."), "uncprb_getname", size(prbmat,"r"));
    error(errmsg)
  end
  if ( rhs < 2 ) then
    longflag = %f
  else
    longflag = varargin(2)
  end
  //
  k = 1
  for nprob = prbmat
    if ( nprob==1 ) then
      if ( longflag == %f ) then
        name(k) = "rose"
      else
        name(k) = "Rosenbrock"
      end
    elseif ( nprob==2 ) then
      if ( longflag == %f ) then
        name(k) = "froth"
      else
        name(k) = "Freudenstein and Roth"
      end
    elseif ( nprob==3 ) then
      if ( longflag == %f ) then
        name(k) = "badscp"
      else
        name(k) = "Powell Badly Scaled"
      end
    elseif ( nprob==4 ) then
      if ( longflag == %f ) then
        name(k) = "badscb"
      else
        name(k) = "Brown Badly Scaled"
      end
    elseif ( nprob==5 ) then
      if ( longflag == %f ) then
        name(k) = "beale"
      else
        name(k) = "Beale"
      end
    elseif ( nprob==6 ) then
      if ( longflag == %f ) then
        name(k) = "jensam"
      else
        name(k) = "Jennrich and Sampson"
      end
    elseif ( nprob==7 ) then
      if ( longflag == %f ) then
        name(k) = "helix"
      else
        name(k) = "Helical Valley"
      end
    elseif ( nprob==8 ) then
      if ( longflag == %f ) then
        name(k) = "bard"
      else
        name(k) = "Bard"
      end
    elseif ( nprob==9 ) then
      if ( longflag == %f ) then
        name(k) = "gauss"
      else
        name(k) = "Gaussian"
      end
    elseif ( nprob==10 ) then
      if ( longflag == %f ) then
        name(k) = "meyer"
      else
        name(k) = "Meyer"
      end
    elseif ( nprob==11 ) then
      if ( longflag == %f ) then
        name(k) = "gulf"
      else
        name(k) = "Gulf Research and Development"
      end
    elseif ( nprob==12 ) then
      if ( longflag == %f ) then
        name(k) = "box"
      else
        name(k) = "Box 3-Dimensional"
      end
    elseif ( nprob==13 ) then
      if ( longflag == %f ) then
        name(k) = "sing"
      else
        name(k) = "Powell Singular"
      end
    elseif ( nprob==14 ) then
      if ( longflag == %f ) then
        name(k) = "wood"
      else
        name(k) = "Wood"
      end
    elseif ( nprob==15 ) then
      if ( longflag == %f ) then
        name(k) = "kowosb"
      else
        name(k) = "Kowalik and Osborne"
      end
    elseif ( nprob==16 ) then
      if ( longflag == %f ) then
        name(k) = "bd"
      else
        name(k) = "Brown and Dennis"
      end
    elseif ( nprob==17 ) then
      if ( longflag == %f ) then
        name(k) = "osb1"
      else
        name(k) = "Osborne 1"
      end
    elseif ( nprob==18 ) then
      if ( longflag == %f ) then
        name(k) = "biggs"
      else
        name(k) = "Biggs EXP6"
      end
    elseif ( nprob==19 ) then
      if ( longflag == %f ) then
        name(k) = "osb2"
      else
        name(k) = "Osborne 2"
      end
    elseif ( nprob==20 ) then
      if ( longflag == %f ) then
        name(k) = "watson"
      else
        name(k) = "Watson"
      end
    elseif ( nprob==21 ) then
      if ( longflag == %f ) then
        name(k) = "rosex"
      else
        name(k) = "Extended Rosenbrock"
      end
    elseif ( nprob==22 ) then
      if ( longflag == %f ) then
        name(k) = "singx"
      else
        name(k) = "Extended Powell Singular"
      end
    elseif ( nprob==23 ) then
      if ( longflag == %f ) then
        name(k) = "pen1"
      else
        name(k) = "Penalty I"
      end
    elseif ( nprob==24 ) then
      if ( longflag == %f ) then
        name(k) = "pen2"
      else
        name(k) = "Penalty II"
      end
    elseif ( nprob==25 ) then
      if ( longflag == %f ) then
        name(k) = "vardim"
      else
        name(k) = "Variably Dimensioned"
      end
    elseif ( nprob==26 ) then
      if ( longflag == %f ) then
        name(k) = "trig"
      else
        name(k) = "Trigonometric"
      end
    elseif ( nprob==27 ) then
      if ( longflag == %f ) then
        name(k) = "almost"
      else
        name(k) = "Brown Almost Linear"
      end
    elseif ( nprob==28 ) then
      if ( longflag == %f ) then
        name(k) = "bv"
      else
        name(k) = "Discrete Boundary Value"
      end
    elseif ( nprob==29 ) then
      if ( longflag == %f ) then
        name(k) = "ie"
      else
        name(k) = "Discrete Integral Equation"
      end
    elseif ( nprob==30 ) then
      if ( longflag == %f ) then
        name(k) = "trid"
      else
        name(k) = "Broyden Tridiagonal"
      end
    elseif ( nprob==31 ) then
      if ( longflag == %f ) then
        name(k) = "band"
      else
        name(k) = "Broyden Banded"
      end
    elseif ( nprob==32 ) then
      if ( longflag == %f ) then
        name(k) = "lin"
      else
        name(k) = "Linear --- Full Rank"
      end
    elseif ( nprob==33 ) then
      if ( longflag == %f ) then
        name(k) = "lin1"
      else
        name(k) = "Linear --- Rank 1"
      end
    elseif ( nprob==34 ) then
      if ( longflag == %f ) then
        name(k) = "lin0"
      else
        name(k) = "Linear - Rank 1 with Zero Cols. & Rows"
      end
    elseif ( nprob==35 ) then
      if ( longflag == %f ) then
        name(k) = "cheb"
      else
        name(k) = "Chebyquad"
      end
    else
      error(msprintf("%s: The input argument nprob is %d but should be between 1 and 35.\n","uncprb_getname",nprob))
    end
    k = k + 1
  end
endfunction

