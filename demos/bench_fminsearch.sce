// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
mprintf("\n==\n");
mprintf("Test fminsearch on these problems.\n")
mprintf("\n==\n");
nprobmax = uncprb_getproblems();
geval = [];
rtolF = 1.e-4;
atolF = 1.e-4;
rtolX = 1.e-4;
atolX = 1.e-6;
nsuccess = 0;
funevalTotal = 0;
tic();
for nprob = 1:nprobmax
    [n,m,x0] = uncprb_getinitf(nprob);
    //
    // Define a wrapper over the objective function for the optimizer
    body = [
    "fout=uncprb_getobjfcn("+string(n)+","+string(m)+",x,"+string(nprob)+")"
    ];
    prot=funcprot();
    funcprot(0);
    deff("fout=objfun(x)",body);
    funcprot(prot);
    //
	fmsopt = optimset ("Display","off", "MaxFunEvals", 5000, "MaxIter", 5000 );
	[xopt,fopt,exitflag,output]=fminsearch(objfun,x0,fmsopt);
    xopt = xopt(:);
    gopt = uncprb_getgrdfcn ( n , m , xopt , nprob );
    funeval = output.funcCount;
    funevalTotal = funevalTotal + funeval;
    iter = output.iterations;
    success = uncprb_printsummary("fminsearch", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
    rtolF, atolF, rtolX, atolX, "wiki");
    if ( success ) then
        nsuccess = nsuccess + 1;
    end
end
t = toc();
mprintf("Time: %d (s)\n",t);
mprintf("Total number of problems:%d\n",nprobmax);
mprintf("Number of successes:%d\n",nsuccess);
mprintf("Number of failures:%d\n",nprobmax-nsuccess);
mprintf("Total Feval:%d\n",funevalTotal);


//
// Manual tuning for one single problem
nprob = 1;
    [n,m,x0] = uncprb_getinitf(nprob);
	[fstar,xstar] = uncprb_getopt(nprob,n,m)
    //
    // Define a wrapper over the objective function for the optimizer
    body = [
    "fout=uncprb_getobjfcn("+string(n)+","+string(m)+",x,"+string(nprob)+")"
    ];
    prot=funcprot();
    funcprot(0);
    deff("fout=objfun(x)",body);
    funcprot(prot);
    //
	//fmsopt = optimset ("PlotFcns" , optimplotfval, "Display","iter","MaxFunEvals", 5000, "MaxIter", 5000);
	fmsopt = optimset ("PlotFcns" , optimplotfval, "MaxFunEvals", 5000, "MaxIter", 5000);
	[xopt,fopt,exitflag,output]=fminsearch(objfun,x0,fmsopt);
	h = gcf();
	h.children.log_flags="nln";
	h.children.children.children.mark_mode="off";
	h.children.children.children.thickness=4;
    xopt = xopt(:);
    gopt = uncprb_getgrdfcn ( n , m , xopt , nprob );
    funeval = output.funcCount;
    funevalTotal = funevalTotal + funeval;
    iter = output.iterations;
    success = uncprb_printsummary("fminsearch", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
    rtolF, atolF, rtolX, atolX, "detailed");


//
// Load this script into the editor
//
filename = "bench_fminsearch.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );

