// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->
// <-- NOT FIXED -->


////////////////////////////////////////

//
mprintf("\n==\n");
mprintf("Test optkelley_ntrust on these problems\n")
mprintf("This requires the optkelley module:\n")
mprintf("atomsInstall(""optkelley"") :\n")
mprintf("Press return to continue\n");
halt()
mprintf("\n==\n");

for nprob = [1:19,23,25,26,28:34]
  [n,m,x0] = uncprb_getinitf(nprob);
  //
  // Define a wrapper over the objective function for the optimizer
  // Caution : transpose the gradient !
  body = [
      "fout=uncprb_getobjfcn("+string(n)+","+string(m)+",x,"+string(nprob)+")"
      "gout=uncprb_getgrdfcn("+string(n)+","+string(m)+",x,"+string(nprob)+")''"
  ];
  prot=funcprot();
  funcprot(0);
  deff("[fout,gout,ind]=objfun(x,ind)",body);
  funcprot(prot);
  tol = 1.e-10;
  [xopt,histout,costdata] = optkelley_ntrust(x0,objfun,tol);
  fopt = uncprb_getobjfcn(n,m,xopt);
  gopt = uncprb_getgrdfcn(n,m,xopt);
  mprintf("Problem #%d, fopt = %e, gopt = %e\n", nprob , fopt , norm(gopt))
end


//
// Load this script into the editor
//
filename = "bench_ntrust.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );

