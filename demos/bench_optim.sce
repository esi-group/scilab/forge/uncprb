// Copyright (C) 2010-2011 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

////////////////////////////////////////

mprintf("\n==\n");
mprintf("Testing optim on several problems in this collection.\n");

//
// Test optim on these problems
// Tolerances on function value
rtolF = 1.e-4;
atolF = 1.e-10;
rtolX = 1.e-4;
atolX = 1.e-6;
msgfmt = "wiki";
mprintf("==================================\n")
mprintf("Compares several approaches with optim:\n")
mprintf(" * optim unbounded with Quasi-Newton (""qn"")\n")
mprintf(" * optim unbounded with L-BFGS (""gc"")\n")
mprintf(" * optim with infinite bounds and Quasi-Newton (""qn"")\n")
mprintf(" * optim with infinite bounds and L-BFGS (""gc"")\n")
mprintf(" * optim without constraints and nonsmooth method (""nd"")\n")
mprintf("==================================\n")
mprintf("Relative Tolerance on F:%e\n",rtolF)
mprintf("Relative Tolerance on X:%e\n",rtolX)
mprintf("Absolute Tolerance on F:%e\n",atolF)
mprintf("Absolute Tolerance on X:%e\n",atolX)
mprintf("==================================\n")


//
// Testing optim / UNC / QN
mprintf("Testing optim UNC ""qn""\n")
nprobmax = uncprb_getproblems();
nsuccess = 0;
funevalTotal = 0;
iter = [];
geval = [];
tic();
for nprob = 1 : nprobmax
    [n,m,x0] = uncprb_getinitf(nprob);
    uncprb_optiminit();
    objfun = list(uncprb_optimfun,nprob,n,m);
    [fopt,xopt,gopt]=optim(objfun,x0,"ar",1000,1000);
    funeval = uncprb_optimget();
    funevalTotal = funevalTotal + funeval;
    success = uncprb_printsummary("optim/UNC/qn", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
    rtolF, atolF, rtolX, atolX, msgfmt);
    if ( success ) then
        nsuccess = nsuccess + 1;
    end
end
t = toc();
mprintf("Time: %d (s)\n",t);
mprintf("Total number of problems:%d\n",nprobmax);
mprintf("Number of successes:%d\n",nsuccess);
mprintf("Number of failures:%d\n",nprobmax-nsuccess);
mprintf("Total Feval:%d\n",funevalTotal);

//
// Test optim / BND / QN
mprintf("==================================\n")
mprintf("Testing optim with infinite BND ""qn""\n")
nsuccess = 0;
funevalTotal = 0;
iter = [];
geval = [];
tic();
for nprob = 1 : nprobmax
    [n,m,x0] = uncprb_getinitf(nprob);
    lb = -%inf*ones(n,1);
    ub = %inf*ones(n,1);
    uncprb_optiminit();
    objfun = list(uncprb_optimfun,nprob,n,m);
    [fopt,xopt,gopt]=optim(objfun,"b",lb,ub,x0,"ar",1000,1000);
    funeval = uncprb_optimget();
    funevalTotal = funevalTotal + funeval;
    success = uncprb_printsummary("optim/BND/qn", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
    rtolF, atolF, rtolX, atolX, msgfmt);
    if ( success ) then
        nsuccess = nsuccess + 1;
    end
end
t = toc();
mprintf("Time: %d (s)\n",t);
mprintf("Total number of problems:%d\n",nprobmax);
mprintf("Number of successes:%d\n",nsuccess);
mprintf("Number of failures:%d\n",nprobmax-nsuccess);
mprintf("Total Feval:%d\n",funevalTotal);

//
// Testing optim / UNC / GC
mprintf("==================================\n")
mprintf("Testing optim UNC ""gc""\n")
nprobmax = uncprb_getproblems();
nsuccess = 0;
funevalTotal = 0;
iter = [];
geval = [];
tic();
for nprob = 1 : nprobmax
    [n,m,x0] = uncprb_getinitf(nprob);
    uncprb_optiminit();
    objfun = list(uncprb_optimfun,nprob,n,m);
    [fopt,xopt,gopt]=optim(objfun,x0,"gc","ar",1000,1000);
    funeval = uncprb_optimget();
    funevalTotal = funevalTotal + funeval;
    success = uncprb_printsummary("optim/UNC/gc", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
    rtolF, atolF, rtolX, atolX, msgfmt);
    if ( success ) then
        nsuccess = nsuccess + 1;
    end
end
t = toc();
mprintf("Time: %d (s)\n",t);
mprintf("Total number of problems:%d\n",nprobmax);
mprintf("Number of successes:%d\n",nsuccess);
mprintf("Number of failures:%d\n",nprobmax-nsuccess);
mprintf("Total Feval:%d\n",funevalTotal);

//
// Testing optim / BND / GC
mprintf("==================================\n")
mprintf("Testing optim with infinite BND ""gc""\n")
nsuccess = 0;
funevalTotal = 0;
iter = [];
geval = [];
tic();
for nprob = 1 : nprobmax
    [n,m,x0] = uncprb_getinitf(nprob);
    lb = -%inf*ones(n,1);
    ub = %inf*ones(n,1);
    uncprb_optiminit();
    objfun = list(uncprb_optimfun,nprob,n,m);
    [fopt,xopt,gopt]=optim(objfun,"b",lb,ub,x0,"gc","ar",1000,1000);
    funeval = uncprb_optimget();
    funevalTotal = funevalTotal + funeval;
    success = uncprb_printsummary("optim/BND/gc", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
    rtolF, atolF, rtolX, atolX, msgfmt);
    if ( success ) then
        nsuccess = nsuccess + 1;
    end
end
t = toc();
mprintf("Time: %d (s)\n",t);
mprintf("Total number of problems:%d\n",nprobmax);
mprintf("Number of successes:%d\n",nsuccess);
mprintf("Number of failures:%d\n",nprobmax-nsuccess);
mprintf("Total Feval:%d\n",funevalTotal);

//
// Testing optim / UNC / ND
mprintf("==================================\n")
mprintf("Testing optim without constraints ""nd""\n")
nsuccess = 0;
funevalTotal = 0;
iter = [];
geval = [];
tic();
for nprob = 1 : nprobmax
    [n,m,x0] = uncprb_getinitf(nprob);
    uncprb_optiminit();
    objfun = list(uncprb_optimfun,nprob,n,m);
    [fopt,xopt,gopt]=optim(objfun,x0,"nd","ar",1000,1000);
    funeval = uncprb_optimget();
    funevalTotal = funevalTotal + funeval;
    success = uncprb_printsummary("optim/BND/gc", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
    rtolF, atolF, rtolX, atolX, msgfmt);
    if ( success ) then
        nsuccess = nsuccess + 1;
    end
end
t = toc();
mprintf("Time: %d (s)\n",t);
mprintf("Total number of problems:%d\n",nprobmax);
mprintf("Number of successes:%d\n",nsuccess);
mprintf("Number of failures:%d\n",nprobmax-nsuccess);
mprintf("Total Feval:%d\n",funevalTotal);

//
// Manual tuning
if ( %f ) then
nprob = 18;
geval = [];
iter = [];
mprintf("==================================\n")
mprintf("Manual tests for problem #%d\n",nprob)
[n,m,x0] = uncprb_getinitf(nprob);
[fstar,xstar] = uncprb_getopt(nprob,n,m)
uncprb_optiminit();
lb = -%inf*ones(n,1);
ub = %inf*ones(n,1);
objfun = list(uncprb_optimfun,nprob,n,m);
[fopt,xopt,gopt]=optim(objfun,x0,"qn","ar",1000,1000);
//[fopt,xopt,gopt]=optim(list(uncprb_optimfun,nprob,n,m),"b",lb,ub,x0,"gc","ar",1000,1000);
funeval = uncprb_optimget();
success = uncprb_printsummary("optim/UNC/tuned",nprob, fopt, xopt, gopt, iter, funeval, geval, ..
rtolF, atolF, rtolX, atolX, "detailed");
// Compute Hessian matrix at this point
H=uncprb_gethesfd(n,m,xopt,nprob);
// If the Hessian is provided:
// H=uncprb_gethesfcn(n,m,xopt,nprob);
disp(spec(H))
end
