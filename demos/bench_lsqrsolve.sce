// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

rtolF = 1.e-4;
atolF = 1.e-10;
rtolX = 1.e-4;
atolX = 1.e-6;

mprintf("==================================\n")
mprintf("Uses lsqrsolve to solve the benchmark\n")
mprintf("==================================\n")
mprintf("Relative Tolerance on F:%e\n",rtolF)
mprintf("Relative Tolerance on X:%e\n",rtolX)
mprintf("Absolute Tolerance on F:%e\n",atolF)
mprintf("Absolute Tolerance on X:%e\n",atolX)
mprintf("==================================\n")

mprintf("Testing lsqrsolve without Jacobian\n")

//stop = [1.e-8,1.e-8,1.e-8,1000,0,100];

passed = 0;
iter = [];
Jeval = [];
nprobmax = uncprb_getproblems();
funevalTotal = 0;
tic();
for nprob = 1 : nprobmax
    [n,m,x0] = uncprb_getinitf(nprob);
    uncprb_lsqrsolveinit();
    objfun = list(uncprb_lsqrsolvefun,nprob,n);
    [xopt, ropt,info]=lsqrsolve(x0,objfun,m);
    gopt = uncprb_getgrdfcn ( n , m , xopt , nprob );
    funeval = uncprb_lsqrsolveget();
    [fstar,xstar] = uncprb_getopt(nprob,n,m);
    fopt = sum(ropt.^2);
    funevalTotal = funevalTotal + funeval;
    success = uncprb_printsummary("lsqrsolve",nprob, fopt, xopt, gopt, iter, funeval, Jeval, ..
    rtolF, atolF, rtolX, atolX, "wiki");
    if (success) then
        passed = passed+1;
    end
end
t = toc();
mprintf("Time: %d (s)\n",t);
mprintf("Total=%d\n",nprobmax);
mprintf("Passed=%d\n",passed);
mprintf("Failed=%d\n",nprobmax-passed);
mprintf("Total Feval:%d\n",funevalTotal);
//////////////////////////////////////////////

mprintf("Testing lsqrsolve with Jacobian\n")

passed = 0;
iter = [];
nprobmax = uncprb_getproblems();
funevalTotal = 0;
JevalTotal = 0;
tic();
for nprob = 1 : nprobmax
    [n,m,x0] = uncprb_getinitf(nprob);
    uncprb_lsqrsolveinit();
    objfun = list(uncprb_lsqrsolvefun,nprob,n);
    jacfun = list(uncprb_lsqrsolvejac,nprob,n);
    [xopt, ropt,info]=lsqrsolve(x0,objfun,m,jacfun);
    gopt = uncprb_getgrdfcn ( n , m , xopt , nprob );
    [funeval,Jeval] = uncprb_lsqrsolveget();
    funevalTotal = funevalTotal + funeval;
    JevalTotal = JevalTotal + Jeval;
    [fstar,xstar] = uncprb_getopt(nprob,n,m);
    fopt = sum(ropt.^2);
    success = uncprb_printsummary("lsqrsolve",nprob, fopt, xopt, gopt, iter, funeval, Jeval, ..
    rtolF, atolF, rtolX, atolX, "wiki");
    if (success) then
        passed = passed+1;
    end
end
t = toc();
mprintf("Time: %d (s)\n",t);
mprintf("Total=%d\n",nprobmax);
mprintf("Passed=%d\n",passed);
mprintf("Failed=%d\n",nprobmax-passed);
mprintf("Total Feval:%d\n",funevalTotal);
mprintf("Total Jeval:%d\n",JevalTotal);
