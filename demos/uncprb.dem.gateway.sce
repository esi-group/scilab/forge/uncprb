// This file is released into the public domain
// This help file was generated using helpupdate at 2010/6/1 - 14:25:43
demopath = get_absolute_file_path("uncprb.dem.gateway.sce");
subdemolist = [
"checkAlgo566_UNC", "checkAlgo566_UNC.sce"; ..
"checkAlgo566_NLEQ", "checkAlgo566_NLEQ.sce"; ..
"bench_problems", "bench_problems.sce"; ..
"bench_optim", "bench_optim.sce"; ..
"bench_lsqrsolve", "bench_lsqrsolve.sce"; ..
"bench_leastsq", "bench_leastsq.sce"; ..
"bench_ntrust", "bench_ntrust.sce"; ..
"bench_fminsearch", "bench_fminsearch.sce"; ..
"bench_bfgswopt", "bench_bfgswopt.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
