
// Copyright INRIA 2008-2009
// Allan CORNET
// Simon LIPP
// Michael Baudin
// This file is released into the public domain

mode(-1);
lines(0);

function uncprbBuildToolbox()
    TOOLBOX_NAME = "uncprb";
    TOOLBOX_TITLE = "Unconstrained Optimization Problems Toolbox";

    toolbox_dir = get_absolute_file_path("builder.sce");

    tbx_builder_macros(toolbox_dir);
    tbx_builder_help(toolbox_dir);
    tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
    tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);
endfunction

uncprbBuildToolbox();
clear uncprbBuildToolbox;

