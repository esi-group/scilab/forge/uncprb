// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

// Get name for Rosenbrock's test case
name = uncprb_getname(1);
assert_checkequal ( name , "rose" );
name = uncprb_getname(1,%f);
assert_checkequal ( name , "rose" );
name = uncprb_getname(1,%t);
assert_checkequal ( name , "Rosenbrock" );

// Check that all names can be retrieved
nprobmax = uncprb_getproblems();
for nprob = 1 : nprobmax
  name = uncprb_getname(nprob);
  assert_checkequal ( typeof(name) , "string" );
  name = uncprb_getname(nprob,%t);
  assert_checkequal ( typeof(name) , "string" );
end

// See a case of error
cmd = "name = uncprb_getname(-1)";
execstr(cmd,"errcatch");
[str,errcode] = lasterror();
expected = "uncprb_getname: The input argument nprob is -1 but should be between 1 and 35.";
assert_checkequal ( str , expected );
//
// Get all names at once
nprobmax = uncprb_getproblems();
name = uncprb_getname(1:nprobmax);
assert_checkequal ( size(name) , [nprobmax 1] );

