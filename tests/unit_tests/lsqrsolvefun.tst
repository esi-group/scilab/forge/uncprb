// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

// Check that the wrapper works fine.
clearglobal();


// Check wrapper on problem #1
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
r = uncprb_lsqrsolvefun ( x0, m, nprob, n );
r0 = [-4.4; 2.2];
assert_checkalmostequal(r0,r);

// Make lsqrsolve optimize the problem #1
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
[xopt, ropt,info]=lsqrsolve(x0,list(uncprb_lsqrsolvefun,nprob,n),m);
assert_checkequal(xopt,[1;1]);
assert_checkequal(ropt,[0;0]);
assert_checkequal(info,2);

// Make lsqrsolve optimize the problem #1
// Get the number of iterations, function and gradient evaluations
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
uncprb_lsqrsolveinit();
objfun = list(uncprb_lsqrsolvefun,nprob,n);
[xopt, ropt,info]=lsqrsolve(x0,objfun,m);
assert_checkequal(xopt,[1;1]);
[funeval,geval] = uncprb_lsqrsolveget();
assert_checktrue(funeval>0);
assert_checkequal(geval,0);


// Make lsqrsolve optimize the problem #1
// Get the number of iterations, function and gradient evaluations
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
uncprb_lsqrsolveinit();
objfun = list(uncprb_lsqrsolvefun,nprob,n);
objjac = list(uncprb_lsqrsolvejac,nprob,n);
[xopt, ropt,info]=lsqrsolve(x0,objfun,m,objjac);
assert_checkequal(xopt,[1;1]);
[funeval,geval] = uncprb_lsqrsolveget();
assert_checktrue(funeval>0);
assert_checktrue(geval>0);


