// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

nprob = 4;
[n,m,x0]=uncprb_getinitf(nprob);
[fopt,xopt,gopt]=optim(list(uncprb_optimfun,nprob,n,m),x0,"gc");
rtolX = 1.e-4;
atolX = 1.e-10;
rtolF = 1.e-4;
atolF = 1.e-10;
[statusF, statusX] = uncprb_computestatus ( nprob, xopt, fopt, ..
rtolF, atolF, rtolX, atolX );
assert_checkequal(statusF,%T);
assert_checkequal(statusX,%T);


