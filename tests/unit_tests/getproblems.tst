// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

//
// Display one problem at a time
str = uncprb_getproblems(1);
expected = [
"No.    file      n    m    Name"
"---    ----      -    -    ----"
"#  1.  ROSE      2    2    Rosenbrock"
  ];
assert_checkequal ( str , expected );
//
// With verbose
str = uncprb_getproblems(1,%t);
expected = [
"No.    file      n    m    Name"
"---    ----      -    -    ----"
"#  1.  ROSE      2    2    Rosenbrock"
  ];
assert_checkequal ( str , expected );
//
// Without verbose
str = uncprb_getproblems(1,%f);
expected = [
"No.    file      n    m    Name"
"---    ----      -    -    ----"
"#  1.  ROSE      2    2    Rosenbrock"
  ];
assert_checkequal ( str , expected );
//
// Display all problems
nprobmax = uncprb_getproblems();
assert_checkequal ( nprobmax , 35 );
for nprob = 1 : nprobmax
  uncprb_getproblems(nprob);
  mprintf("\n");
end

// Display all problems in one call
nprobmax = uncprb_getproblems();
uncprb_getproblems(1:nprobmax);

//
// Get all problems
nprobmax = uncprb_getproblems();
str = uncprb_getproblems(1:nprobmax,%f);
expected = [
"No.    file      n    m    Name"
"---    ----      -    -    ----"
"#  1.  ROSE      2    2    Rosenbrock"
"#  2.  FROTH     2    2    Freudenstein and Roth"
"#  3.  BADSCP    2    2    Powell Badly Scaled"
"#  4.  BADSCB    2    3    Brown Badly Scaled"
"#  5.  BEALE     2    3    Beale"
"#  6.  JENSAM    2    (10) Jennrich and Sampson"
"#  7.  HELIX     3    3    Helical Valley"
"#  8.  BARD      3    15   Bard"
"#  9.  GAUSS     3    15   Gaussian"
"# 10.  MEYER     3    16   Meyer"
"# 11.  GULF      3    (99) Gulf Research and Development"
"# 12.  BOX       3    (10) Box 3-Dimensional"
"# 13.  SING      4    4    Powell Singular"
"# 14.  WOOD      4    6    Wood"
"# 15.  KOWOSB    4    11   Kowalik and Osborne"
"# 16.  BD        4    (20) Brown and Dennis"
"# 17.  OSB1      5    33   Osborne 1"
"# 18.  BIGGS     6    (13) Biggs EXP6"
"# 19.  OSB2      11   65   Osborne 2"
"# 20.  WATSON    (9)  31   Watson"
"# 21.  ROSEX     (20) (20) Extended Rosenbrock"
"# 22.  SINGX     (12) (12) Extended Powell Singular"
"# 23.  PEN1      (10) (11) Penalty I"
"# 24.  PEN2      (10) (20) Penalty II"
"# 25.  VARDIM    (10) (12) Variably Dimensioned"
"# 26.  TRIG      (10) (10) Trigonometric"
"# 27.  ALMOST    (10) (10) Brown Almost Linear"
"# 28.  BV        (8)  (8)  Discrete Boundary Value"
"# 29.  IE        (8)  (8)  Discrete Integral Equation"
"# 30.  TRID      (12) (12) Broyden Tridiagonal"
"# 31.  BAND      (15) (15) Broyden Banded"
"# 32.  LIN       (10) (15) Linear --- Full Rank"
"# 33.  LIN1      (10) (15) Linear --- Rank 1"
"# 34.  LIN0      (10) (15) Linear - Rank 1 with Zero Cols. & Rows"
"# 35.  CHEB      (10) (10) Chebyquad"
];
expected = stripblanks(expected);
str = stripblanks ( str );
assert_checkequal ( str , expected );

