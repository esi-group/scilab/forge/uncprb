// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

// Get starting data for Rosenbrock's test case
// n=2, m=2, x=[-1.2,1]'
[n,m,x]=uncprb_getinitpt(1);
assert_checkequal ( n , 2 );
assert_checkequal ( m , 2 );
assert_checkalmostequal ( x , [-1.2,1]', %eps );

// Get starting data for Rosenbrock's test case
// Multiplies x0 by 3
[n,m,x]=uncprb_getinitpt(1,3);
assert_checkequal ( n , 2 );
assert_checkequal ( m , 2 );
assert_checkalmostequal ( x , 3*[-1.2,1]', %eps );


