// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

// Use default
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
Hfd = uncprb_gethesfd ( n , m , x0 , nprob );
E = [
    1330.    480.
    480.     200.
];
assert_checkalmostequal ( Hfd , E , 1.e-8 );

// Use default
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
Hfd = uncprb_gethesfd ( n , m , x0 , nprob , [] , [] );
E = [
    1330.    480.
    480.     200.
];
assert_checkalmostequal ( Hfd , E , 1.e-8 );

// Set the step
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
Hfd = uncprb_gethesfd ( n , m , x0 , nprob , 1.e-1 );
E = [
    1330.    480.
    480.     200.
];
assert_checkalmostequal ( Hfd , E , 1.e-2 );

// Set the step and the order
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
Hfd = uncprb_gethesfd ( n , m , x0 , nprob , 1.e-1 , 4 );
E = [
    1330.    480.
    480.     200.
];
assert_checkalmostequal ( Hfd , E , 1.e-14 );

// Set the order
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
Hfd = uncprb_gethesfd ( n , m , x0 , nprob , [] , 4 );
E = [
    1330.    480.
    480.     200.
];
assert_checkalmostequal ( Hfd , E , 1.e-12 );


