Unconstrained Optimization Problem Toolbox

Purpose
-------

The goal of this toolbox is to provide unconstrained optimization problems
in order to test optimization algorithms.

The More, Garbow and Hillstrom collection of test functions is widely used
in testing unconstrained optimization software.  The code for these problems
is available in Fortran from the netlib software archives.

The port from Fortran to Matlab was done by two undergraduate students at Brooklyn College,
Livia Klein and Madhu Lamba, under the supervision of Chaya Gurwitz.

Benoit Hamelin did the port from Matlab to Scilab v4 with m2sci and
did some manual tuning of the result.

Michael Baudin did the port from Scilab v4 to Scilab v5.
I renamed the functions to avoid naming conflicts.
I formatted the help pages to generate automatically the xml from the
sources.

 * Provides 35 unconstrained optimization problems.
 * Provides the function value, the gradient, the function vector, the Jacobian.
 * Provides the Hessian matrix for 18 problems.
 * Provides the starting point for each problem.
 * Provides the optimum function value and the optimum point x for many problems.
 * Provides finite difference routines for the gradient, the Jacobian and the Hessian matrix.
 * Macro based functions : no compiler required.
 * All function values, gradients, Jacobians and Hessians are tested.


Features
--------

 * uncprb_overview — An overview of the Unconstrained Optimization Problems toolbox.
 * uncprb_getclass — Returns the name.
 * uncprb_getfunc — Returns the function vector, the Jacobian and, if available, the Hessian.
 * uncprb_getgrdfcn — Returns the gradient.
 * uncprb_getgrdfd — Compute the gradient by finite differences.
 * uncprb_gethesfcn — Returns the function value.
 * uncprb_gethesfd — Compute the Hessian by finite differences.
 * uncprb_getinitf — Returns the starting point.
 * uncprb_getinitpt — Returns the starting point.
 * uncprb_getname — Returns the name.
 * uncprb_getobjfcn — Returns the function value.
 * uncprb_getopt — Returns the name.
 * uncprb_getproblems — Lists the problems.
 * uncprb_getvecfcn — Returns the function vector and the Jacobian.
 * uncprb_getvecjac — Returns the Jacobian.
 * uncprb_getvecjacfd — Compute the Jacobian by finite differences
 * Benchmark
   * uncprb_computestatus — Computes the status of an optimization.
   * uncprb_leastsqfun — Returns the residual vector for the leastsq function.
   * uncprb_leastsqget — Get the fields of the internal data for uncprb_leastsqfun.
   * uncprb_leastsqinit — Initialize the internal data for uncprb_leastsqfun
   * uncprb_leastsqjac — Returns the Jacobian matrix for the leastsq function.
   * uncprb_lsqrsolvefun — Returns the residual vector for the lsqrsolve function.
   * uncprb_lsqrsolveget — Get the fields of the internal data for uncprb_lsqrsolve.
   * uncprb_lsqrsolveinit — Initialize the internal data for uncprb_lsqrsolve
   * uncprb_lsqrsolvejac — Returns the Jacobian matrix for the lsqrsolve function.
   * uncprb_optimfun — Returns f and g for the optim function.
   * uncprb_optimget — Get the fields of the internal data for uncprb_optimfun.
   * uncprb_optiminit — Initialize the internal data for uncprb_optimfun
   * uncprb_printsummary — rtolF, atolF, rtolX, atolX, fmt)

Demos
-----

 * checkAlgo566_UNC : compare the output of uncprb against Algo 566 on UNC problems
 * checkAlgo566_NLEQ : compare the output of uncprb against Algo 566 on NLEQ problems
 * bench_problems : Compare performances of the functions of the test problems
 * bench_optim : Test the optim function on this benchmark.
 * bench_lsqrsolve : Test the lsqrsolve function on this benchmark.
 * bench_leastsq : Test the leastsq function on this benchmark.
 * bench_ntrust : Test the ntrust function (from optkelley) on this benchmark.
 * bench_fminsearch : Test the fminsearch function on this benchmark.
 * bench_bfgswopt : Test the bfgswopt function (from optkelley) on this benchmark.


Problems
--------
The following problems are available.

No.    file      n    m    Name
---    ----      -    -    ----
#  1.  ROSE      2    2    Rosenbrock
#  2.  FROTH     2    2    Freudenstein and Roth
#  3.  BADSCP    2    2    Powell Badly Scaled
#  4.  BADSCB    2    3    Brown Badly Scaled
#  5.  BEALE     2    3    Beale
#  6.  JENSAM    2    (10) Jennrich and Sampson
#  7.  HELIX     3    3    Helical Valley
#  8.  BARD      3    15   Bard
#  9.  GAUSS     3    15   Gaussian
# 10.  MEYER     3    16   Meyer
# 11.  GULF      3    (10) Gulf Research and Development
# 12.  BOX       3    (10) Box 3-Dimensional
# 13.  SING      4    4    Powell Singular
# 14.  WOOD      4    6    Wood
# 15.  KOWOSB    4    11   Kowalik and Osborne
# 16.  BD        4    (20) Brown and Dennis
# 17.  OSB1      5    33   Osborne 1
# 18.  BIGGS     6    (13) Biggs EXP6
# 19.  OSB2      11   65   Osborne 2
# 20.  WATSON    (10) 31   Watson
# 21.  ROSEX     (20) (20) Extended Rosenbrock
# 22.  SINGX     (12) (12) Extended Powell Singular
# 23.  PEN1      (8)  (9)  Penalty I
# 24.  PEN2      (8)  (16) Penalty II
# 25.  VARDIM    (10) (12) Variably Dimensioned
# 26.  TRIG      (10) (10) Trigonometric
# 27.  ALMOST    (10) (10) Brown Almost Linear
# 28.  BV        (8)  (8)  Discrete Boundary Value
# 29.  IE        (8)  (8)  Discrete Integral Equation
# 30.  TRID      (12) (12) Broyden Tridiagonal
# 31.  BAND      (15) (15) Broyden Banded
# 32.  LIN       (10) (15) Linear --- Full Rank
# 33.  LIN1      (10) (15) Linear --- Rank 1
# 34.  LIN0      (10) (15) Linear - Rank 1 with Zero Cols. & Rows
# 35.  CHEB      (10) (10) Chebyquad

See the overview in the help provided with this toolbox.

Dependencies
------------
 * When updating the help pages, this module requires the helptbx module.
 * This module requires the assert module (for the unit tests).

TODO:
-----
 * update implementation and vectorize when possible

References
----------

"Algorithm 566: FORTRAN Subroutines for Testing Unconstrained Optimization Software"
ACM Transactions on Mathematical Software (TOMS)
Volume 7 ,  Issue 1, March 1981
Pages: 136 - 140
J. J. Moré, Burton S. Garbow, Kenneth E. Hillstrom

"HESFCN - A Fortran Package Of Hessian
Subroutines For Testing Nonlinear Optimization
Software".
Victoria Averbukh, Samuel Figueroa, And Tamar Schlick
Courant Institue Of Mathematical Sciences

http://www.dmi.usherb.ca/~hamelin/autodiff/html/sciad_en.html

Author
------

Scilab v5 port and update: 2010, Michael Baudin
Scilab port: 2000-2004, Benoit Hamelin, Jean-Pierre Dussault
Matlab port: Chaya Gurwitz, Livia Klein, and Madhu Lamba
2000 - John Burkardt (optimum points for problem #20)
Fortran 77: Jorge More, Burton Garbow and Kenneth Hillstrom

Licence

This toolbox is released under the terms of the GNU LGPL license.


